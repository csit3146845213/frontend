export const baseUrl = process.env.NODE_ENV === "production" ? "https://tradielog.arichernando.com/backend" : "http://localhost:3000"
export const Config = {
  dev: {
    BASE_URL: "http://localhost:3000",
  },
};

