import { createTheme } from "@mui/material/styles";
import { blue, red } from "@mui/material/colors";


declare module "@mui/material/Button" {
  interface ButtonPropsColorOverrides {
    button: true;
    danger: true;
    accept: true;
    neutral: true;
  }
}

declare module "@mui/material/styles" {
  interface Palette {
    button: Palette[ "primary" ];
    danger: Palette[ "primary" ];
    accept: Palette[ "primary" ];
    neutral: Palette[ "primary" ];
  }
  interface PaletteOptions {
    button: PaletteOptions[ "primary" ];
    danger: PaletteOptions[ "primary" ];
    accept: PaletteOptions[ "primary" ];
    neutral: PaletteOptions[ "primary" ];
  }
}

// https://mui.com/material-ui/customization/theming/
const theme = createTheme({
  palette: {
    primary: {
      main: "#000000",
    },
    secondary: {
      main: "#FFFFFF",
    },
    error: {
      main: red.A400,
    },
    info: {
      main: "#C7DBE7"
    },
    danger: {
      main: "#F57E7E"
    },
    button: {
      main: "#C7DBE7"
    },
    accept: {
      main: "#ADD88B"
    },
    neutral: {
      main: "#FFFFFF"
    }
  },

  typography: {
    button: {
      textTransform: "none",
    },
  },
});

export default theme;
