import React from "react";
import ReactDOM from "react-dom/client";
import { ThemeProvider } from "@mui/material/styles";
import "./index.css";
import theme from "./theme";
import { CssBaseline } from "@mui/material";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import NotFound from "./pages/NotFound/NotFound";
import Home from "./pages/Home/Home";
import App from "./App";
import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";
import Login from "./pages/Auth/Login";
import AuthProvider from "./context/AuthContext";
import BreadcrumbProvider from "./context/BreadcrumbContext";
import ProSignupBreadcrumb from "./pages/Auth/ProSignup/ProSignupBreadcrumb";
import BecomeProfessional from "./pages/BecomeProfessional/BecomeProfessional";
import Membership from "./pages/Membership/Membership";
import ClientProfile from "./pages/Profile/ClientProfile";
import ServiceRequestList from "./pages/Request/ServiceRequestList";
import ServiceSelection from "./pages/Request/ServiceSelection";
import ClientOrders from "./pages/Services/ClientOrders";
import ClientRequestForm from "./pages/Services/ClientRequestForm";
import ProViewsRequest from "./pages/Services/ProViewsRequest";
import ProfessionalOrders from "./pages/Services/ProOrders";
import ServiceOffer from "./pages/Services/ServiceOffer";
import Client from "./components/Private/Client";
import Pro from "./components/Private/Pro";
import ProfessionalProfileBreadcrumb from "./pages/Profile/ProProfile/ProProfileBreadcrumb";
import ClientSignup from "./pages/Auth/ClientSignup";
import ProSignupProfile from "./pages/Auth/ProSignup/ProSignupProfile";
import AddRating from "./pages/Rating/AddRating";
import ViewRating from "./pages/Rating/ViewRating";
import ProSignupSkills from "./pages/Auth/ProSignup/ProSignupSkills";
import ProOnboarding from "./pages/Home/ProOnboarding";
import ProSignupAddCard from "./pages/Auth/ProSignup/ProSignupAddCard";
import Address from "./components/Address/Address";
import Payment from "./pages/Payment/Payment";
import ProSkills from "./pages/Profile/ProProfile/ProSkills";
import ProProfile from "./pages/Profile/ProProfile/ProProfile";
import LoadingProvider from "./context/LoadingContext";
import SnackbarProvider from "./context/SnackbarContext";
import AdminPage from "./pages/Admin/AdminPage";
import Admin from "./components/Private/Admin";
import PayForMembership from "./pages/Membership/PayForMembership";
import AddCreditCardForm from "./pages/Payment/AddCreditCardForm";

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <BrowserRouter>
        <AuthProvider>
          <BreadcrumbProvider>
            <LoadingProvider>
              <SnackbarProvider>
                <Routes>
                  <Route element={<App />}>
                    <Route path="/" element={<Home />} />
                    <Route path="/login" element={<Login />} />
                    <Route path="/fakeadmin" element={<AdminPage />} />
                    <Route path="/admin" element={<Admin><AdminPage /></Admin>} />

                    <Route path="/client">
                      <Route index element={<Home />} />
                      <Route path="book" element={<Client><ClientRequestForm /></Client>} />
                      <Route path="profile" element={<Client><ClientProfile /></Client>} />
                      <Route path="register" element={<ClientSignup />} />
                      <Route path="orders" element={<Client><ClientOrders /></Client>} />
                      <Route path="services" element={<Client><ServiceSelection /></Client>} />
                      <Route path="offer" element={<Client><ServiceOffer /></Client>} />
                      <Route path="add-rating" element={<Client><AddRating /></Client>} />
                      <Route path="membership" element={<Client><Membership /></Client>} />
                    </Route>

                    {/* Routes only accessible by pros */}
                    <Route path="/pro">
                      <Route index element={<BecomeProfessional />} />
                      {/* TODO: Show seller logged in after register success */}
                      <Route path="register" element={<ProSignupBreadcrumb />}>
                        <Route index element={<Navigate to="personal" />} />
                        <Route path="personal" element={<ProSignupProfile />} />
                        <Route path="skills" element={<ProSignupSkills />} />
                        <Route path="payment" element={<ProSignupAddCard />} />
                      </Route>
                      <Route path="profile" element={<Pro><ProfessionalProfileBreadcrumb /></Pro>}>
                        <Route index element={<Navigate to="personal" />} />
                        <Route path="personal" element={<ProProfile />} />
                        <Route path="skills" element={<ProSkills />} />
                      </Route>
                      <Route path="onboarding" element={<ProOnboarding />} />
                      <Route path="rating" element={<Pro><ViewRating /></Pro>} />
                      {/* getAllNearbyClients */}
                      <Route path="request" element={<Pro><ServiceRequestList /></Pro>} />
                      {/* getSpecificRequestData */}
                      <Route path="request/:id" element={<Pro><ProViewsRequest /></Pro>} />
                      {/* getAllRequestedJobs, getAllOngoing, getAllFinished */}
                      <Route path="orders" element={<Pro><ProfessionalOrders /></Pro>} />
                    </Route>

                    {/* Combine these three pages */}
                    <Route path="membership" element={<Membership />} />
                    <Route path="pay-membership" element={<PayForMembership />} />

                    {/* TODO: Combine this page and make a general payment page */}
                    <Route path="payment" element={<Payment />} />
                    <Route path="add-card" element={<AddCreditCardForm />} />
                  </Route>
                  <Route path="*" element={<NotFound />} />
                </Routes>
              </SnackbarProvider>
            </LoadingProvider>
          </BreadcrumbProvider>
        </AuthProvider>
      </BrowserRouter>
    </ThemeProvider>
  </React.StrictMode>
);
