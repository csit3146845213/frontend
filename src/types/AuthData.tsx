const defaultState: AuthState = {
  headers: {},
  isAuth: false,
  userData: {
    email: "",
    username: "",
  },
  userType: "Professional",
};
class AuthState {
  constructor(
    public headers: {
      Authorization?: string;
    },
    public isAuth: boolean,
    public userType: UserTypes,
    public userData: {
      [ key: string ]: any
    },
  ) { }
  static createFromString(json: string): AuthState {
    if (!json || json === "") {
      return defaultState;
    }
    try {
      return JSON.parse(json) as AuthState;
    } catch {
      return defaultState
    }
  }
}

export type UserTypes = "Professional" | "Client" | "Admin"

export default AuthState;
