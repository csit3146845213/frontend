import "./App.css";
import { Outlet } from "react-router-dom";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import Breadcrumb from "./components/Breadcrumb/Breadcrumb";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import SnackBar from "./components/SnackBar/SnackBar";
import { SetStateAction } from "react";


function App() {
  return (
    <>
      <Header></Header>
      <SnackBar></SnackBar>
      <Breadcrumb></Breadcrumb>
      {/* Output component */}
      <Outlet></Outlet>
      <Footer></Footer>
    </>
  );
}

export default App;
