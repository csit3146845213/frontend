import { Button } from "@mui/material";
import RequestDetails from "../../components/RequestDetails/RequestDetails";
import { useLocation } from "react-router-dom";
import { useState } from "react";
import { GenericForm } from "../../types/GenericForm";
import createNewServiceRequest from "../../services/services/createNewServiceRequest";
import useHttpRequest from "../../hooks/useHttpRequest";

export default function ClientRequestForm() {
  const { state } = useLocation()
  const { serviceTitle, selectedTime } = state

  const [ requestInfo, setRequestInfo ] = useState({
    service_name: serviceTitle,
    service_request_date: selectedTime
  } as GenericForm)

  const onClick = useHttpRequest(() => createNewServiceRequest(requestInfo), () => { }, "Created new service")

  return (
    <RequestDetails requestInfo={requestInfo} setRequestInfo={setRequestInfo} title="Request Details" readonly={false}>
      <Button variant="contained" onClick={onClick} sx={{ marginTop: "12px" }}>
        Send Request
      </Button>
    </RequestDetails>
  );
}
