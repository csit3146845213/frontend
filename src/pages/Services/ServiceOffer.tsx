import {
  Avatar,
  Button,
  Container,
  Divider,
  IconButton,
  TextField,
  Typography,
} from "@mui/material";
import RequestDetails from "../../components/RequestDetails/RequestDetails";
import { useEffect, useState } from "react";
import { GenericForm } from "../../types/GenericForm";
import useHttpRequest from "../../hooks/useHttpRequest";
import clientRespondOffer from "../../services/services/clientRespondOffer";
import { useLocation, useNavigate } from "react-router-dom";

export default function ServiceOffer() {
  const { state } = useLocation()

  const [ offerData, setOfferData ] = useState({} as GenericForm)
  const [ buttonDisabled, setButtonDisabled ] = useState(false)
  const navigate = useNavigate()
  const firstName = `${(offerData.serviceOffers ?? [ { first_name: " " } ])[ 0 ].first_name}`
  const lastname = `${(offerData.serviceOffers ?? [ { last_name: "" } ])[ 0 ].last_name}`
  const pro_id = (offerData.serviceOffers ?? [ { professional_id: 0 } ])[ 0 ].professional_id
  useEffect(() => {
    if (!state) navigate("/")
    const { cardData } = state
    setOfferData(cardData)
  }, [])

  const acceptOffer = useHttpRequest(() => clientRespondOffer(offerData.service_id, pro_id, true), () => {
    setButtonDisabled(true)
  }, "Succesfully accepted offer")
  const declineOffer = useHttpRequest(() => clientRespondOffer(offerData.service_id, pro_id, false), () => {
    setButtonDisabled(true)
  }, "Succesfully declined offer")
  return (
    <Container>
      <Typography variant="h6" textAlign={"left"} sx={{ my: "24px" }}>
        {`${offerData.service_name ?? ""} service offer by ${firstName} ${lastname}`}
      </Typography>
      <RequestDetails
        title={`Offer Price : $${(offerData.serviceOffers ?? [ { offer_price: 0 } ])[ 0 ].offer_price}`}
        readonly={true}
        requestInfo={offerData}
        setRequestInfo={setOfferData}
      >
        <Button disabled={buttonDisabled} variant="contained" onClick={acceptOffer} sx={{ marginTop: "12px" }}>
          Accept Request
        </Button>
        <Button disabled={buttonDisabled} variant="contained" onClick={declineOffer} sx={{ marginTop: "12px" }}>
          Decline Request
        </Button>
        <Button color="neutral" variant="contained" sx={{ marginTop: "12px" }}>
          Go back
        </Button>
      </RequestDetails>
      <Typography textAlign={"left"} fontWeight={"bold"} sx={{ my: "12px" }}>
        {`About ${(offerData.serviceOffers ?? [ { first_name: "" } ])[ 0 ].first_name} ${(offerData.serviceOffers ?? [ { last_name: "" } ])[ 0 ].last_name}`}
      </Typography>
      <Typography textAlign={"justify"} sx={{ mb: "24px" }}>
        {`${(offerData.serviceOffers ?? [ { description: "" } ])[ 0 ].description}`}
      </Typography>
      <Container
        sx={{
          border: "solid 1px black",
          padding: "24px",
          my: "12px",
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <IconButton sx={{ p: 0, mx: "20px" }}>
          <Avatar sx={{ width: 60, height: 60 }}>{firstName[ 0 ]}</Avatar>
        </IconButton>
        <Typography>{`${firstName} ${lastname}`}</Typography>
        <Typography sx={{ mx: "28px" }} textAlign={"left"}>
          Skills
          <br />
          {(offerData.serviceOffers ?? [ { skills: "" } ])[ 0 ].skills}
        </Typography>
      </Container>
    </Container>
  );
}
