import { Button, Divider, TextField } from "@mui/material";
import RequestDetails from "../../components/RequestDetails/RequestDetails";
import { useNavigate, useParams, useSearchParams } from "react-router-dom";
import { ChangeEvent, useEffect, useState } from "react";
import getSpecificService from "../../services/services/getSpecificService";
import { GenericForm } from "../../types/GenericForm";
import validator from "validator";
import proRespondRequest from "../../services/services/proRespondRequest";
import { useLoading } from "../../context/LoadingContext";
import { useSnackbar } from "../../context/SnackbarContext";
import useHttpRequest from "../../hooks/useHttpRequest";
import useHttpEffect from "../../hooks/useHttpEffect";

export default function ProViewsRequest() {
  let { id } = useParams()
  const navigate = useNavigate()

  const { isLoading, setIsLoading } = useLoading()
  const { snackbar, setSnackbar } = useSnackbar()
  const [ price, setPrice ] = useState(0)
  const [ error, setError ] = useState("")
  const [ requestInfo, setRequestInfo ] = useState({} as GenericForm)
  useEffect(() => {
    id = id ?? ""
    if (id.length == 0) navigate("/")
    setIsLoading(true)
    getSpecificService(id)
      .then((value) => {
        setRequestInfo(value)
      }).catch((value) => {
        setSnackbar({ message: "Failed to get service", severity: "error" })
      }).finally(() => {
        setIsLoading(false)
      })
  }, [])


  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setError("")
    if (!validator.isCurrency(event.target.value)) setError("Invalid price")
    else setPrice(Number(event.target.value))
  };

  const acceptRequest = useHttpRequest(() => {
    setError("")
    if (!validator.isCurrency(String(price)) || price <= 0) {
      setError("Invalid price");
      setIsLoading(false)
      return Promise.reject()
    }
    return proRespondRequest({
      accept: true,
      service_id: id,
      offer_price: price
    })
  }, () => {
    navigate("/pro/orders")
  })

  const declineRequest = useHttpRequest(() => {
    setError("")
    if (!validator.isCurrency(String(price))) { setError("Invalid price"); return Promise.reject() }
    return proRespondRequest({
      accept: false,
      service_id: id,
      offer_price: price
    })
  }, value => {
    navigate("/pro/orders")
  })


  return (
    <RequestDetails
      title="Request Details"
      readonly={true}
      requestInfo={requestInfo}
      setRequestInfo={setRequestInfo}
    >
      <Divider sx={{ my: "30px" }}></Divider>
      <TextField
        required
        error={error.length > 0}
        helperText={error}
        variant="outlined"
        name="price"
        label="Service Price"
        onChange={handleChange}
        InputProps={{ startAdornment: "$" }}
      />
      <Button color="accept" onClick={acceptRequest} variant="contained" sx={{ marginTop: "12px" }}>
        Accept request
      </Button>
      <Button color="danger" onClick={declineRequest} variant="contained" sx={{ marginTop: "12px" }}>
        Decline request
      </Button>
      <Button color="neutral" onClick={() => { navigate(-1) }} variant="contained" sx={{ marginTop: "12px" }}>
        Go back
      </Button>
    </RequestDetails>
  );
}
