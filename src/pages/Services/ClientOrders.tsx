import { Box, Button, Container, Typography } from "@mui/material";
import ServiceCard from "../../components/ServiceCard/ServiceCard";
import { useEffect, useState } from "react";
import { GenericForm } from "../../types/GenericForm";
import ServiceGrid from "../../components/ServiceGrid/ServiceGrid";
import RequestedServiceCard from "../../components/ServiceCard/RequestedServiceCard";
import getServiceList from "../../services/services/getServiceList";
import ServiceOfferCard from "../../components/ServiceCard/ServiceOfferCard";
import OngoingServiceCard from "../../components/ServiceCard/OngoingServiceCard";
import ClientFinishedServiceCard from "../../components/ServiceCard/ClientFinishedServiceCard";
import { useLoading } from "../../context/LoadingContext";
import { useSnackbar } from "../../context/SnackbarContext";

export default function ClientOrders() {
  const { isLoading, setIsLoading } = useLoading()
  const { snackbar, setSnackbar } = useSnackbar()
  const [ requestedServices, setRequestedServices ] = useState<GenericForm[]>([]);
  const [ serviceOffers, setServiceOffers ] = useState<GenericForm[]>([]);
  const [ ongoingServices, setOngoingServices ] = useState<GenericForm[]>([]);
  const [ finishedServices, setFinishedServices ] = useState<GenericForm[]>([]);

  useEffect(() => {
    setIsLoading(true)
    getServiceList("Requested")
      .then((value) => {
        setRequestedServices(value)
        setIsLoading(false)

      }).catch(() => {
        setSnackbar({ message: "Failed to get request data", severity: "error" })
      })

    getServiceList("Offered")
      .then((value) => {
        console.log(value)
        setServiceOffers(value)
      })

    getServiceList("OnGoing")
      .then((value) => {
        setOngoingServices(value)
      })

    getServiceList("Finished")
      .then((value) => {
        setFinishedServices(value)
      })
  }, [])

  useEffect(() => {
    setIsLoading(true)
    getServiceList("OnGoing")
      .then((value) => {
        setOngoingServices(value)
      }).finally(() => {
        setIsLoading(false)
      })
  }, [ JSON.stringify(serviceOffers) ])

  useEffect(() => {
    setIsLoading(true)
    getServiceList("Finished")
      .then((value) => {
        setIsLoading(false)
        setFinishedServices(value)
      })
  }, [ JSON.stringify(ongoingServices) ])

  return (
    <Box sx={{ my: "24px", minHeight: "100vh" }}>
      <ServiceGrid title="Requested Services" serviceData={requestedServices} cardType={RequestedServiceCard} setServiceData={setRequestedServices} />
      <ServiceGrid title="Service Offers" serviceData={serviceOffers} cardType={ServiceOfferCard} setServiceData={setServiceOffers} />
      <ServiceGrid title="Ongoing Services" serviceData={ongoingServices} cardType={OngoingServiceCard} setServiceData={setOngoingServices} />
      <ServiceGrid title="Finished Services" serviceData={finishedServices} cardType={ClientFinishedServiceCard} setServiceData={setFinishedServices} />
    </Box>
  );
}
