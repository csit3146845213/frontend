import { Box, Button, Container, Typography } from "@mui/material";
import ServiceCard from "../../components/ServiceCard/ServiceCard";
import { useEffect, useState } from "react";
import ServiceGrid from "../../components/ServiceGrid/ServiceGrid";
import { GenericForm } from "../../types/GenericForm";
import OngoingServiceCard from "../../components/ServiceCard/OngoingServiceCard";
import ClientFinishedServiceCard from "../../components/ServiceCard/ClientFinishedServiceCard";
import getServiceList from "../../services/services/getServiceList";
import { useSnackbar } from "../../context/SnackbarContext";
import { useLoading } from "../../context/LoadingContext";
import OfferCard from "../../components/ServiceCard/OfferCard";
import ProOngoingServiceCard from "../../components/ServiceCard/ProOngoingServiceCard";
import ProFinishedServiceCard from "../../components/ServiceCard/ProFinishedServiceCard";

export default function ProfessionalOrders() {
  const { isLoading, setIsLoading } = useLoading()
  const { snackbar, setSnackbar } = useSnackbar()

  const [ outstandingOffers, setOutstandingOffers ] = useState<GenericForm[]>([])
  const [ ongoingServices, setOngoingServices ] = useState<GenericForm[]>([])
  const [ finishedServices, setFinishedServices ] = useState<GenericForm[]>([])

  useEffect(() => {
    setIsLoading(true)

    getServiceList("OnGoing")
      .then((value) => {
        setOngoingServices(value)
      }).catch(() => {
        setSnackbar({ message: "Failed to get request data", severity: "error" })
      }).finally(
        () => { setIsLoading(false) }
      )

    getServiceList("Finished")
      .then((value) => {
        setFinishedServices(value)
      }).finally(
        () => { setIsLoading(false) }
      )

    getServiceList("Offered")
      .then((value) => {
        setOutstandingOffers(value)
      }).finally(
        () => { setIsLoading(false) }
      )
  }, [])


  return (
    <Box sx={{ my: "24px", minHeight: "100vh" }}>
      <ServiceGrid title="Outstanding Offers" serviceData={outstandingOffers} cardType={OfferCard} setServiceData={setOutstandingOffers} />
      <ServiceGrid title="Ongoing Services" serviceData={ongoingServices} cardType={ProOngoingServiceCard} setServiceData={setOngoingServices} />
      <ServiceGrid title="Finished Services" serviceData={finishedServices} cardType={ProFinishedServiceCard} setServiceData={setFinishedServices} />
    </Box>
  );
}
