import { Box, Button, Container, TextField, Typography } from "@mui/material";
import { CartesianGrid, Cell, Label, Legend, Line, LineChart, Pie, PieChart, Tooltip, XAxis, YAxis } from "recharts";
import React, { PureComponent, useEffect, useState } from 'react';
import dayjs from "dayjs";
import useHttpEffect from "../../hooks/useHttpEffect";
import paymentReports from "../../services/admin/paymentReports";
import serviceReports from "../../services/admin/serviceReports";
import { GenericForm } from "../../types/GenericForm";

const dataCircle = [
  { name: 'Group A', value: 400 },
  { name: 'Group B', value: 300 },
  { name: 'Group C', value: 300 },
  { name: 'Group D', value: 200 },
  { name: 'Group E', value: 278 },
  { name: 'Group F', value: 189 },
];

const data = [
  { name: 'A', uv: 400 },
  { name: 'B', uv: 500 },
  { name: 'C', uv: 600 },
  { name: 'D', uv: 800 },
  { name: 'E', uv: 1000 },
  { name: 'A', uv: 1200 },
];

const COLORS = [ '#0088FE', '#00C49F', '#FFBB28', '#FF8042' ];
const RADIAN = Math.PI / 180;
export default function AdminPage() {
  const [ timeError, setTimeError ] = useState("")
  const [ selectedMinTime, setSelectedMinTime ] = useState(dayjs().subtract(1, 'year').toISOString())
  const [ selectedMaxTime, setSelectedMaxTime ] = useState(dayjs().add(1, 'year').toISOString())
  const [ paymentData, setPaymentData ] = useState([])
  const [ serviceData, setServiceData ] = useState([])

  useHttpEffect(paymentReports(selectedMinTime, selectedMaxTime), value => {
    setPaymentData(value.summary ?? [])
  }, [ selectedMinTime, setSelectedMinTime ])

  useHttpEffect(serviceReports(selectedMinTime, selectedMaxTime), value => {
    setServiceData(value.summary ?? [])
  }, [ selectedMinTime, setSelectedMinTime ])

  return (
    <Container>
      <Typography textAlign={"left"} sx={{ my: "12px", fontSize: "28px" }} variant="h6">Dashboard</Typography>
      <Typography textAlign={"left"} sx={{ my: "12px", fontWeight: "bold" }}>Total Revenue</Typography>
      <Box display={"flex"} alignItems={"center"}>
        <TextField
          type="date"
          label="From"
          value={dayjs(selectedMinTime).format("YYYY-MM-DD")}
          onChange={(event) => {
            console.log(event.target.value)
            setSelectedMinTime(dayjs(event.target.value).toISOString())
          }}
          InputLabelProps={{
            shrink: true
          }}
          sx={{ m: "12px" }}
          error={timeError.length > 0}
          helperText={timeError}
          inputProps={{
            min: new Date().toISOString().slice(0, 16),
          }}
        />
        <TextField
          type="date"
          label="To"
          onChange={(event) => {
            setSelectedMaxTime(dayjs(event.target.value).toISOString())
          }}
          value={dayjs(selectedMaxTime).format("YYYY-MM-DD")}
          InputLabelProps={{
            shrink: true
          }}
          sx={{ m: "12px" }}
          error={timeError.length > 0}
          helperText={timeError}
          inputProps={{
            min: new Date().toISOString().slice(0, 16),
          }}
        />
      </Box>
      <LineChart width={500} height={300} data={paymentData}>
        <XAxis dataKey="date">
          <Label value="Date " offset={0} position="insideBottom" />
        </XAxis>
        <YAxis dataKey="revenue">
          <Label value="Gross Revenue " offset={0} position="insideLeft" angle={-90} />
        </YAxis>
        <CartesianGrid stroke="#eee" strokeDasharray="5 5" />
        <Line type="monotone" dataKey="revenue" stroke="#8884d8" />
        <Tooltip />
      </LineChart>
      <Typography textAlign={"left"} sx={{ mt: "12px", fontWeight: "bold" }}>Number of Request</Typography>

      <PieChart width={400} height={400}>
        <Legend layout="vertical" verticalAlign="middle" align="right" />
        <Pie
          data={serviceData}
          cx="50%"
          cy="50%"
          labelLine={false}
          outerRadius={80}
          fill="#8884d8"
          dataKey="count"
        >
          {serviceData.map((entry: GenericForm, index) => {
            return (
              <Cell name={entry.service_name} key={`cell-${index}`} fill={COLORS[ index % COLORS.length ]} />
            )

          }
          )}
        </Pie>
      </PieChart>
    </Container>
  )
}
