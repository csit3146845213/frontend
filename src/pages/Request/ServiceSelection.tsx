import {
  Box,
  Button,
  Container,
  Grid,
  TextField,
  Typography,
} from "@mui/material";
import ServiceCard from "../../components/ServiceCard/ServiceCard";
import { serviceList } from "./ServiceList";
import { ChangeEvent, FormEvent, MouseEventHandler, useState } from "react";
import { useNavigate } from "react-router-dom";
import serviceAssets from "../../assets/serviceAssets/serviceAssets";

export default function ServiceSelection() {
  const [ selectedTime, setSelectedTime ] = useState("")
  const [ timeError, setTimeError ] = useState("")
  const navigate = useNavigate()

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setSelectedTime(event.target.value)
    setTimeError("")
  }

  const onClick = (serviceTitle: string) => {
    if (selectedTime === "") {
      setTimeError("Please insert date of booking")
      return
    }
    navigate("/client/book", { state: { serviceTitle, selectedTime } })
  }

  return (
    <Container sx={{ my: "24px", textAlign: "left" }}>
      <Typography textAlign={"left"} variant="h6" sx={{ my: "12px" }}>
        Requested Services
      </Typography>
      <TextField
        type="datetime-local"
        error={timeError.length > 0}
        helperText={timeError}
        value={selectedTime}
        onChange={handleChange}
        inputProps={{
          min: new Date().toISOString().slice(0, 16),
        }}
      />
      <Grid container spacing={2} sx={{ mt: "12px" }}>
        {serviceList.map((value) => {
          return (
            <Grid key={value} item xs={3}>
              <ServiceCard image={serviceAssets[ value ]} values={{}} title={value}>
                <Button sx={{ mt: "36px", mx: "12px" }} onClick={() => {
                  onClick(value);
                }}>Book Service</Button>
              </ServiceCard>
            </Grid>
          )
        })}

      </Grid>
    </Container>
  );
}
