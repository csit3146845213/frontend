import {
  Box,
  Button,
  Container,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  Typography,
} from "@mui/material";
import ServiceCard from "../../components/ServiceCard/ServiceCard";
import { useEffect, useState } from "react";
import getNearbyServices from "../../services/services/getNearbyServices";
import { useNavigate } from "react-router-dom";
import { GenericForm } from "../../types/GenericForm";
import ServiceGrid from "../../components/ServiceGrid/ServiceGrid";
import NearbyServiceCard from "../../components/ServiceCard/NearbyServiceCard";
import useHttpEffect from "../../hooks/useHttpEffect";

export default function ServiceRequestList() {
  const [ radius, setRadius ] = useState(50);
  const [ nearbyServices, setNearbyServices ] = useState<GenericForm[]>([]);

  const handleChange = (event: SelectChangeEvent) => {
    setRadius(parseInt(event.target.value));
  };

  useHttpEffect(getNearbyServices(radius), value => {
    setNearbyServices(value)
  }, [ radius ])

  return (
    <Box sx={{ my: "24px", flexGrow: "1" }}>
      <Container sx={{ textAlign: "left" }}>
        <Typography variant="h6" sx={{ my: "12px" }}>
          Requested Services by Client
        </Typography>
        <FormControl sx={{ minWidth: "150px", textAlign: "left" }}>
          <InputLabel>Location Radius</InputLabel>
          <Select
            value={radius.toString()}
            label="Location Radius"
            onChange={handleChange}
          >
            <MenuItem value={10}>5km</MenuItem>
            <MenuItem value={25}>25km</MenuItem>
            <MenuItem value={50}>50km</MenuItem>
          </Select>
        </FormControl>
        <ServiceGrid serviceData={nearbyServices} setServiceData={setNearbyServices} cardType={NearbyServiceCard}></ServiceGrid>
      </Container>
    </Box>
  );
}
