import React, { useEffect } from "react";
import {
  Box,
  Container,
  ImageList,
  ImageListItem,
  ImageListItemBar,
  Typography,
} from "@mui/material";
import homeAssets, { HomeAssets } from "../../assets/homeAssets";
import { useAuth } from "../../context/AuthContext";
import { useNavigate } from "react-router-dom";

function Home() {
  console.log(process.env.NODE_ENV)
  const { authState, setAuthState } = useAuth()
  const navigate = useNavigate()
  useEffect(() => {
    if (authState.userType === "Admin") navigate("/admin")
  }, [])
  return (
    <Box>
      {/* Box for the image */}
      <Box sx={{ height: "60vh", width: "100%", position: "relative" }}>
        <img
          src={homeAssets.homepageBig}
          alt="Half page image"
          style={{ width: "100%", height: "100%", objectFit: "cover" }}
        />
      </Box>

      {/* Text Box below the image */}
      <Box
        sx={{
          bgcolor: "#C7DBE7",
          height: "8vh",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Typography
          variant="h6"
          component="div"
          sx={{
            color: "#000",
            fontSize: "24px",
            fontFamily: "inherit",
            fontStyle: "italic",
          }}
        >
          Find the perfect professional service for your house
        </Typography>
      </Box>

      <Container maxWidth={false}>
        <Box
          sx={{
            bgcolor: "#fff",
            height: "5vh",
            width: "30%",
            display: "flex",
            alignItems: "left",
            marginTop: "5%",
            marginBottom: "0%",
          }}
        >
          <Typography
            variant="h5"
            component="div"
            sx={{
              color: "#000",
              fontSize: "24px",
              fontFamily: "inherit",
              fontWeight: "bold",
            }}
          >
            Popular Professional Services
          </Typography>
        </Box>
        <ImageList variant="masonry" cols={5} gap={8}>
          {itemData.map((item) => (
            <ImageListItem key={item.img}>
              <img
                src={`${item.img}?w=248&fit=crop&auto=format`}
                srcSet={`${item.img}?w=248&fit=crop&auto=format&dpr=2 2x`}
                alt={item.title}
                loading="lazy"
                style={{ height: "320px" }}
              />
              <ImageListItemBar
                position="below"
                title={item.title}
                sx={{
                  "& .MuiImageListItemBar-title": {
                    width: "100%",
                    fontSize: "21px",
                  }, //styles for title
                  //styles for subtitle
                }} // Set width to 100%
              />
            </ImageListItem>
          ))}
        </ImageList>
      </Container>
    </Box>
  );
}

const itemData = [
  {
    img: homeAssets.treeRemoval,
    title: "Tree Removal",
    author: "Author 2",
  },
  {
    img: homeAssets.roofCleaning,
    title: "Roof Cleaning",
    author: "Author 3",
  },
  {
    img: homeAssets.fenceInstallation,
    title: "Fence Installation",
  },
  {
    img: homeAssets.ovenRepair,
    title: "Oven Repairs",
    author: "Author 3",
  },
  {
    img: homeAssets.lawnMowing,
    title: "Lawn Mowing",
    author: "Author 3",
  },
  // add more items as needed
];

export default Home;
