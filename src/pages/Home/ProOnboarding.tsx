import { Box, Container, IconButton, Typography } from '@mui/material';
import React, { useState } from 'react';
import { SearchOutlined, ArticleOutlined, SettingsSuggestOutlined } from '@mui/icons-material';

function ProOnboarding() {
  // Hook part, needs to be understand more
  const [ sellerName, setSellerName ] = useState('Seller 123');
  const handleInputChange = (event: { target: { value: React.SetStateAction<string>; }; }) => {
    setSellerName(event.target.value);
  }
  return (
    // Beware, the margin is manually set, if changes needed, needs to see the color box again 
    <Container maxWidth="xl">
      {/* // Bold Text to welcome user */}
      <Box sx={{ bgcolor: 'white', height: '5vh', width: '30%', display: 'flex', alignItems: 'left', marginTop: '5%', marginLeft: '17vh' }}>
        <Typography variant="h4" component="div" sx={{ color: '#000', fontSize: '24px', fontFamily: 'inherit', fontWeight: 'bold' }}>
          Welcome {sellerName}
        </Typography>
      </Box>
      {/* Text below Welcome User */}
      <Box sx={{ bgcolor: 'white', height: '5vh', width: '30%', display: 'flex', alignItems: 'left', marginLeft: '17vh' }}>
        <Typography variant="h4" component="div" sx={{ color: '#000', fontSize: '24px', fontFamily: 'inherit' }}>
          How TradiesPros Work
        </Typography>
      </Box>

      {/* Icon Section */}
      <Box sx={{ bgcolor: '#white', height: '20vh', width: '200vh', display: 'flex', alignItems: 'center', justifyContent: 'flex-start', paddingLeft: '16px', margin: 'auto', marginTop: '5vh' }}>
        <SearchOutlined sx={{ fontSize: 200, margin: '22vh 17vh 20vh' }} />
        <ArticleOutlined sx={{ fontSize: 200, margin: '22vh 25vh 20vh' }} />
        <SettingsSuggestOutlined sx={{ fontSize: 200, margin: '22vh 15vh 20vh' }} />
      </Box>

      {/* Text Below Icon */}
      <Box sx={{ bgcolor: '#white', height: '20vh', width: '200vh', display: 'flex', alignItems: 'center', justifyContent: 'flex-start', paddingLeft: '16px', margin: 'auto', marginBottom: '15vh' }}>
        <Typography variant="h5" sx={{ color: '#000', width: '40vh', height: '10vh', margin: '10vh 15vh 10vh' }}>
          <Box component="span" fontWeight='bold'>1.</Box> To get started, check for client request  in the request tabs
        </Typography>
        <Typography variant="h5" sx={{ color: '#000', width: '40vh', height: '10vh', margin: '10vh 15vh 10vh' }}>
          <Box component="span" fontWeight='bold'>2.</Box> Accepted service request by both party will appear in orders tab
        </Typography>
        <Typography variant="h5" sx={{ color: '#000', width: '40vh', height: '10vh', margin: '10vh 15vh 10vh' }}>
          <Box component="span" fontWeight='bold'>3.</Box> Professional Service can commenced!
        </Typography>
      </Box>
    </Container>
  )
}

export default ProOnboarding;
