import { Button } from "@mui/material";
import RatingForm from "../../components/RatingForm/RatingForm";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import useHttpRequest from "../../hooks/useHttpRequest";
import getSpecificReview from "../../services/rating/getSpecificReview";
import { GenericForm } from "../../types/GenericForm";

export default function ViewRating() {
  const navigate = useNavigate();
  const { state } = useLocation();
  const { service_id, pro_id } = state
  const [ reviewData, setReviewData ] = useState({} as GenericForm)

  useEffect(() => {
    if (!(state ?? {}).service_id) navigate("/")
    console.log(state)
  }, [])

  useEffect(
    useHttpRequest(() => getSpecificReview(pro_id, service_id), (value: GenericForm) => {
      setReviewData(value.reviews[ 0 ])
    }, "", true,), [])
  return (
    <RatingForm readonly={true} title={"Review"} values={reviewData}>
      <Button
        variant="contained"
        onClick={() => {
          navigate(-1);
        }}
      >
        Go Back
      </Button>
    </RatingForm>
  );
}
