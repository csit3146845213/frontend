import { Button } from "@mui/material";
import RatingForm from "../../components/RatingForm/RatingForm";
import { FormEventHandler, useEffect, useState } from "react";
import { GenericForm } from "../../types/GenericForm";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import useHttpRequest from "../../hooks/useHttpRequest";
import postReview from "../../services/rating/postReview";

export default function AddRating() {
  const { state } = useLocation();
  const { service_id, pro_id } = state
  const [ formData, setFormData ] = useState<GenericForm>({ rating: 0 });
  const [ buttonDisabled, setButtonDisabled ] = useState(false)
  const navigate = useNavigate()

  const onClick = useHttpRequest(() => postReview({
    pro_id: pro_id,
    service_id: service_id,
    review: formData.review,
    rating: formData.rating
  }), () => {
    setButtonDisabled(true)
  })


  useEffect(() => {
    if (!(state ?? {}).service_id) navigate("/")

  }, [])

  return (
    <RatingForm
      values={formData}
      setValues={setFormData}
      title={"Leave a Review"}
      readonly={buttonDisabled}
    >
      <Button onClick={onClick} disabled={buttonDisabled} variant="contained" color="primary" >
        Submit
      </Button>
    </RatingForm>
  );
}
