import { Button, Snackbar } from "@mui/material";
import PersonalInfo from "../../components/PersonalInfo/PersonalInfo";
import { FormEvent, FormEventHandler, useState } from "react";
import { GenericForm } from "../../types/GenericForm";
import { useAuth } from "../../context/AuthContext";
import proUpdateProfile from "../../services/profile/updateProfile";
import SnackBar from "../../components/SnackBar/SnackBar";
import { useSnackbar } from "../../context/SnackbarContext";
import { useLoading } from "../../context/LoadingContext";
import clientUpdateProfile from "../../services/profile/clientUpdateProfile";

export default function PersonalProfile() {
  const { authState, setAuthState } = useAuth()
  const [ personalInfo, setPersonalInfo ] = useState<GenericForm>(authState.userData as GenericForm)
  const { snackbar, setSnackbar } = useSnackbar()
  const { isLoading, setIsLoading } = useLoading()

  // TODO
  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    setIsLoading(true)
    clientUpdateProfile(personalInfo)
      .then((value) => {
        setSnackbar({ message: "Profile updated", severity: "success" })
        setAuthState((prev) => {
          return {
            ...prev,
            userData: value
          }
        })
      }).catch(() => {

        setSnackbar({ message: "Failed to update profile", severity: "error" })
      })
  }

  return (
    <PersonalInfo personalInfo={personalInfo} setPersonalInfo={setPersonalInfo} disableEmail={true} onSubmit={handleSubmit}>
      <Button color="button" variant="contained" type="submit" sx={{ marginTop: "12px" }}>
        Update
      </Button>
    </PersonalInfo>
  );
}
