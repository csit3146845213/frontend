import { Button } from "@mui/material";
import { FormEvent, FormEventHandler, useState } from "react";
import PersonalInfo from "../../../components/PersonalInfo/PersonalInfo";
import { useAuth } from "../../../context/AuthContext";
import proUpdateProfile from "../../../services/profile/updateProfile";
import { GenericForm } from "../../../types/GenericForm";
import { useFormContext } from "../../../components/FormContainer/FormContainer";
import { useNavigate } from "react-router-dom";

export default function ProProfile() {
  const { authState, setAuthState } = useAuth()
  const [ personalInfo, setPersonalInfo ] = useFormContext()
  const navigate = useNavigate();

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    navigate("/pro/profile/skills")
  }

  return (
    <PersonalInfo personalInfo={personalInfo} setPersonalInfo={setPersonalInfo} disableEmail={true} onSubmit={handleSubmit}>
      <Button color="button" variant="contained" type="submit" sx={{ marginTop: "12px" }}>
        Update
      </Button>
    </PersonalInfo>
  );
}
