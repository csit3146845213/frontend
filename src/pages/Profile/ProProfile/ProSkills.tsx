import { Button } from "@mui/material";
import ProfessionalInfo from "../../../components/ProfessionalInfo/ProfessionalInfo";
import { useOutletContext } from "react-router-dom";
import { GenericForm } from "../../../types/GenericForm";
import { ChangeEvent, FormEvent, useEffect, useState } from "react";
import { useFormContext } from "../../../components/FormContainer/FormContainer";
import proUpdateProfile from "../../../services/profile/updateProfile";
import { useAuth } from "../../../context/AuthContext";
import { useSnackbar } from "../../../context/SnackbarContext";

export default function ProSkills() {
  const { authState, setAuthState } = useAuth()
  const { snackbar, setSnackbar } = useSnackbar()
  const [ formData, setFormData ] = useFormContext()
  const [ skillList, setSkillList ] = useState(new Set<string>(formData.skills) ?? new Set<string>());

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    proUpdateProfile(formData, skillList)
      .then((value) => {
        setAuthState((prev) => {
          return {
            ...prev,
            userData: value
          }
        })
        setSnackbar({ message: "Profile updated", severity: "success" })
      }).catch(() => {
        setSnackbar({ message: "Failed to update profile", severity: "error" })
      })
  };

  return (
    <ProfessionalInfo onSubmit={handleSubmit} formData={formData} setFormData={setFormData} setSkillList={setSkillList} skillList={skillList}>
      <Button color="button" variant="contained" type="submit" sx={{ marginTop: "12px" }}>
        Update
      </Button>
    </ProfessionalInfo>
  );
}
