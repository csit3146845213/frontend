import { useEffect, useState } from "react";
import { Outlet, useLocation, useNavigate, useOutletContext } from "react-router-dom";
import { useBreadcrumb } from "../../../context/BreadcrumbContext";
import { useAuth } from "../../../context/AuthContext";
import { GenericForm } from "../../../types/GenericForm";

// Use steppers here for multistep sign up
export default function ProfessionalProfileBreadcrumb() {
  const { breadcrumbs, setBreadcrumbState } = useBreadcrumb();
  const { authState, setAuthState } = useAuth();
  const [ formData, setFormData ] = useState(authState.userData as GenericForm)

  const location = useLocation();

  const defaultTrail: { [ key: string ]: string } = {
    "/pro/profile/personal": "Personal Info",
    "/pro/profile/skills": "Professional Info"
  };

  useEffect(() => {
    setBreadcrumbState(defaultTrail);
    return () => {
      setBreadcrumbState({});
    };
  }, [ location ]);

  return (
    <Outlet context={[ formData, setFormData ]}></Outlet>
  );
}
