import { Container, Box, Button, Typography, ImageList, ImageListItem } from '@mui/material';
import becomeProAssets from '../../assets/becomeProAssets';
import { useNavigate } from 'react-router-dom';

export default function BecomeProfessional() {
  const navigate = useNavigate();
  return <Container maxWidth={false}>
    <Box sx={{ alignItems: 'left', display: 'flex', marginTop: '3%', marginLeft: '10%', marginBottom: '3%' }}>
      <Typography variant="h6" component="div" sx={{ color: '#000', fontSize: '24px', fontFamily: 'inherit', fontWeight: 'bold' }}>
        Join our growing freelance community
      </Typography>
    </Box>

    <Box sx={{ marginLeft: '10%', marginRight: '10%' }}>
      <ImageList variant="masonry" cols={4} gap={30}>
        {itemData.map((item) => (
          <ImageListItem key={"image-item-" + item.img}>
            <img
              src={`${item.img}?w=248&fit=crop&auto=format`}
              srcSet={`${item.img}?w=248&fit=crop&auto=format&dpr=2 2x`}
              alt={item.title}
              loading="lazy"
            />
            {item.title === 'blue' && (
              <Box sx={{ width: '100%', height: '100%', position: 'absolute', top: 0, left: 0, display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                <Box sx={{ width: '100%', height: '100%', backgroundColor: '#C7DBE7', paddingLeft: '10%', paddingRight: '10%', textAlign: 'center', display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
                  <Typography variant="h5" sx={{ fontWeight: 'bold'}}>What's your Skill?</Typography>
                  <Box sx={{ marginTop: '3%' }}>
                    <Button variant="outlined" sx={{ backgroundColor: 'white' }} onClick={() => { navigate("/pro/register") }}>Become a Seller</Button>
                  </Box>
                </Box>
              </Box>
            )}
          </ImageListItem>
        )
        )}

        {/* <Box sx={{ width: 225, height: 225, maxHeight: 225, maxWidth: 225, backgroundColor: '#C7DBE7', paddingLeft: '10%', paddingRight: '10%'}}>
            <Box sx={{textAlign: 'center', position: 'relative', top: '50%', transform: 'translateY(-50%)'}}>
              <Typography variant="h5" sx={{fontWeight: 'bold'}}>
              What's your Skill?
              </Typography>
              
            </Box>
            <Box sx={{marginTop:'3%',textAlign: 'center', position: 'relative', top: '50%', transform: 'translateY(-50%)'}}>
              
            <Button variant="outlined" sx={{backgroundColor: 'white'}}>Become a Seller</Button>
              
            </Box>
            
          </Box> */}


      </ImageList>
    </Box>


  </Container>


}

const itemData = [
  {
    img: becomeProAssets.prof1,
    title: 'Prof1'
  },
  {
    img: becomeProAssets.prof2,
    title: 'Prof2'
  },
  {
    img: becomeProAssets.prof3,
    title: 'Prof3'
  },
  {
    img: becomeProAssets.prof4,
    title: 'Prof4'
  },
  {
    img: becomeProAssets.prof5,
    title: 'Prof5'
  },
  {
    img: becomeProAssets.prof6,
    title: 'Prof6'
  },
  {
    img: becomeProAssets.prof7,
    title: 'Prof7'
  },
  {
    img: becomeProAssets.prof1,
    title: 'blue'
  }
]
