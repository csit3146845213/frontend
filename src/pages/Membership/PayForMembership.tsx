import { useState } from "react";
import PaymentAddCard from "../../components/Payment/PaymentAddCard";
import { GenericForm } from "../../types/GenericForm";
import PaymentFilledOut from "../Payment/PaymentFilledOut";
import useHttpEffect from "../../hooks/useHttpEffect";
import getCard from "../../services/payment/getCard";
import useHttpRequest from "../../hooks/useHttpRequest";
import payForMembership from "../../services/payment/payForMembership";
import { CreditCardProps } from "../../components/Payment/CreditCardInfo";
import { useSnackbar } from "../../context/SnackbarContext";
import { useAuth } from "../../context/AuthContext";

export default function PayForMembership() {
  const [ cardList, setCardList ] = useState([] as GenericForm[]);
  const [ currentCard, setCurrentCard ] = useState({} as GenericForm)
  const { snackbar, setSnackbar } = useSnackbar()
  const { authState, setAuthState } = useAuth()
  useHttpEffect(getCard(), value => {
    setCardList(value.cards ?? [])
  }, [])

  const clickCard = (cardProps: CreditCardProps) => {
    console.log(cardProps)
    payForMembership(cardProps)
      .then(() => {
        setSnackbar({ message: "Succesfully paid for membership", severity: "success" })
      }).catch(() => {
        setSnackbar({ message: "Failed to pay for membership", severity: "error" })
      })
  }

  const handleSubmit = useHttpRequest(() => payForMembership(currentCard), (value) => {
    setAuthState((prev) => {
      return {
        ...prev,
        userData: {
          ...prev.userData,
          membership: true
        }
      }
    })

  }, "Succesfully paid for membership!")

  return cardList.length > 0 ?
    <PaymentFilledOut card={cardList} setCardList={setCardList} onClickCard={clickCard}></PaymentFilledOut> :
    <PaymentAddCard buttonText="Purchase" formData={currentCard} setFormData={setCurrentCard} onSubmit={handleSubmit} />
}
