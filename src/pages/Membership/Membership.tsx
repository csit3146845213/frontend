import { Container, Box, Button } from "@mui/material";
import ContainerForm from "../../components/FormContainer/FormContainer";
import NotSubscribedContainer from "../../components/NotSubscribedContainer/NotSubscribedContainer";
import membershipAssets, { MembershipAssets } from "../../assets/membershipAssets";
import { useAuth } from "../../context/AuthContext";
import NotFound from "../NotFound/NotFound";
import { useState } from "react";

export default function Membership() {
  const { authState, setAuthState } = useAuth();
  const [ membership, setMembership ] = useState(Boolean(authState.userData.membership))
  console.log(authState)
  if (authState.userType === "Client" && !membership) {
    return (
      <NotSubscribedContainer isAuth={authState.isAuth} title={"TradieLog Membership"} subtitle={"Home services done by professional"} imageUrl={membershipAssets.membership1}>
        <Box sx={{ alignItems: "start", textAlign: "left" }}>
          Get the most benefit out of TradieLog membership with
          extra added perks and features!
        </Box>
      </NotSubscribedContainer>
    );
  }

  else if (authState.userType == "Professional" && !membership) {
    return (
      <NotSubscribedContainer
        isAuth={authState.isAuth}
        title={"TradieLog Membership"}
        subtitle={"Provide the best services with extra perks"}
        imageUrl={membershipAssets.membership2}
      >
        <Box sx={{ alignItems: "start", textAlign: "left" }}>
          Get the most benefit out of TradieLog membership with
          extra added perks and features!
        </Box>
      </NotSubscribedContainer>
    );
  }

  else if (membership) {
    return (
      <ContainerForm title={"Membership"}>
        <Box
          sx={{
            textAlign: "left",
            lineHeight: 2.5,
          }}
        >
          <Box
            sx={{
              lineHeight: 1.5,
            }}
          >
            Hello user, <br />
            Thanks for being a Tradies Member
            <br />
          </Box>
          <br />
          <Box
            sx={{
              lineHeight: 1.5,
            }}
          >
            {`Next Billing date: ${new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toLocaleDateString()}`}
            <br />
          </Box>
        </Box>
      </ContainerForm>
    );
  }

  else {
    return <NotFound></NotFound>
  }

}
