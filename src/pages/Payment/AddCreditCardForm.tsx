
import { ChangeEvent, Dispatch, SetStateAction, useState } from "react";
import PaymentAddCard from "../../components/Payment/PaymentAddCard";
import { GenericForm } from "../../types/GenericForm";
import useHttpRequest from "../../hooks/useHttpRequest";
import addCard from "../../services/payment/addCard";
import { useNavigate } from "react-router-dom";

function AddCreditCardForm() {
  const [ formData, setFormData ] = useState({} as GenericForm)
  const navigate = useNavigate()
  const handleSubmit = useHttpRequest(() => addCard(formData), () => {
    navigate(-1)
  })

  return (
    <PaymentAddCard formData={formData} setFormData={setFormData} onSubmit={handleSubmit}></PaymentAddCard >
  );
}

export default AddCreditCardForm;
