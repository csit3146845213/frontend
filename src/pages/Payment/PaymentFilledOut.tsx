import { Box, Button } from "@mui/material";
import CreditCardInfo, { CreditCardProps } from "../../components/Payment/CreditCardInfo";
import FormContainer from "../../components/FormContainer/FormContainer";
import { GenericForm } from "../../types/GenericForm";
import { Dispatch, SetStateAction } from "react";
import { useNavigate } from "react-router-dom";

interface Props {
  card: GenericForm[]
  setCardList: Dispatch<SetStateAction<GenericForm[]>>
  onClickCard: (value: CreditCardProps) => void
}

export default function PaymentFilledOut({ card, setCardList, onClickCard }: Props) {
  const navigate = useNavigate()
  return (
    <FormContainer title={"Registered Card"}>
      <Box
        sx={{
          textAlign: "left",
          lineHeight: 2.5,
        }}
      >
        {card.map((creditCard, index) => (
          <CreditCardInfo onClickCard={onClickCard} key={index} creditCardNumber={String(creditCard.card_num ?? "")} expiryDate={creditCard.expire_date ?? ""} name={creditCard.first_name ?? ""} />
        ))}

        <Button
          variant="contained"
          color="button"
          onClick={() => {
            navigate("/add-card")
          }}
          sx={{
            marginTop: 2,
            width: 300,
          }}
        >
          Add Card
        </Button>
      </Box>
    </FormContainer>
  );
}
