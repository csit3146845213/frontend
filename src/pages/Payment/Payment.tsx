import { useEffect, useState } from "react";
import AddCreditCardForm from "./AddCreditCardForm";
import PaymentFilledOut from "./PaymentFilledOut";
import useHttpEffect from "../../hooks/useHttpEffect";
import getCard from "../../services/payment/getCard";
import { GenericForm } from "../../types/GenericForm";

export default function Payment() {
  const [ cardList, setCardList ] = useState([] as GenericForm[]);
  useHttpEffect(getCard(), value => {
    setCardList(value.cards ?? [])

  }, [])
  return cardList.length > 0 ?
    <PaymentFilledOut card={cardList} setCardList={setCardList} onClickCard={() => { }}></PaymentFilledOut> :
    <AddCreditCardForm></AddCreditCardForm>
}
