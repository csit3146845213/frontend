import { Box, Button, Container, TextField, Typography } from "@mui/material";
import { useAuth } from "../../context/AuthContext";
import AuthState from "../../types/AuthData";
import { ChangeEvent, FormEvent, useState } from "react";
import { Outlet, useNavigate } from "react-router-dom";
import PasswordForm from "../../components/PasswordForm/PasswordForm";
import FormContainer from "../../components/FormContainer/FormContainer";
import clientRegister from "../../services/register/clientRegister";
import { GenericForm } from "../../types/GenericForm";
import { infolist } from "../../components/PersonalInfo/PersonalInfoList";
import formatNumberInput from "../../hooks/formatNumberInput";
import Signup from "../../components/Signup/Signup";

function ClientSignup() {
  const { authState, setAuthState } = useAuth();
  const [ formData, setFormData ] = useState({} as GenericForm);

  const navigate = useNavigate();

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    clientRegister(formData)
      .then((value) => {
        setAuthState(value as AuthState)
      })
      .then(() => navigate("/"));
  };


  return (
    <Signup formData={formData} setFormData={setFormData} onSubmit={handleSubmit}>
      <Button color="button" variant="contained" type="submit" sx={{ mt: "12px" }}>
        Sign Up
      </Button>
    </Signup>
  );
}

export default ClientSignup;
