import { useState } from "react";
import { GenericForm } from "../../../types/GenericForm";
import PaymentAddCard from "../../../components/Payment/PaymentAddCard";
import { useFormContext } from "../../../components/FormContainer/FormContainer";
import proRegister from "../../../services/register/proRegister";
import { useNavigate } from "react-router-dom";
import { useAuth } from "../../../context/AuthContext";
import AuthState from "../../../types/AuthData";
import useHttpRequest from "../../../hooks/useHttpRequest";
import addCard from "../../../services/payment/addCard";

function ProSignupAddCard() {
  const [ formData, setFormData ] = useFormContext()
  const { authState, setAuthState } = useAuth()
  const navigate = useNavigate()

  const handleSubmit = useHttpRequest(() => proRegister(formData), (value) => {
    setAuthState(value)
    navigate("/")
  })
  return (
    <PaymentAddCard formData={formData} setFormData={setFormData} onSubmit={handleSubmit}></PaymentAddCard>
  );
}

export default ProSignupAddCard;
