import { Box, Button, Container, TextField, Typography } from "@mui/material";
import { ChangeEvent, FormEvent, useState } from "react";
import { Outlet, useNavigate, useOutletContext } from "react-router-dom";
import PasswordForm from "../../../components/PasswordForm/PasswordForm";
import FormContainer, { useFormContext } from "../../../components/FormContainer/FormContainer";
import Signup from "../../../components/Signup/Signup";

function ProSignupProfile() {
  const [ formData, setFormData ] = useFormContext()
  const navigate = useNavigate()
  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    navigate("/pro/register/skills")
  };

  return (
    <Signup formData={formData} onSubmit={handleSubmit} setFormData={setFormData}>
      <Button variant="contained" type="submit" sx={{ mt: "12px" }}>
        Continue
      </Button>
    </Signup>
  );
}

export default ProSignupProfile;
