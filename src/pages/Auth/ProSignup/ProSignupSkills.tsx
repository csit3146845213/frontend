import { Button } from "@mui/material";
import { useNavigate, useOutletContext } from "react-router-dom";
import { FormEvent, useEffect, useState } from "react";
import { useFormContext } from "../../../components/FormContainer/FormContainer";
import ProfessionalInfo from "../../../components/ProfessionalInfo/ProfessionalInfo";

export default function ProSignupSkills() {
  const [ formData, setFormData ] = useFormContext()
  const [ skillList, setSkillList ] = useState<Set<string>>(new Set(formData.skills) ?? new Set<string>());
  const navigate = useNavigate()

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    setFormData((prev) => {
      return {
        ...formData,
        skills: skillList
      }
    })
    navigate("/pro/register/payment")
  };

  return (
    <ProfessionalInfo onSubmit={handleSubmit} formData={formData} setFormData={setFormData} setSkillList={setSkillList} skillList={skillList}>
      <Button variant="contained" type="submit" sx={{ marginTop: "12px" }}>
        Continue
      </Button>
    </ProfessionalInfo>
  );
}
