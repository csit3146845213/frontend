import { createContext, useEffect, useState } from "react";
import { Outlet, useLocation, useNavigate, useOutletContext } from "react-router-dom";
import { useBreadcrumb } from "../../../context/BreadcrumbContext";
import { useAuth } from "../../../context/AuthContext";
import { GenericForm } from "../../../types/GenericForm";

// Use steppers here for multistep sign up
export default function ProSignupBreadcrumb() {
  const { breadcrumbs, setBreadcrumbState } = useBreadcrumb();
  const { authState, setAuthState } = useAuth();
  const [ formData, setFormData ] = useState({} as GenericForm);
  const navigate = useNavigate();

  const location = useLocation();
  const defaultTrail: { [ key: string ]: string } = {
    "/pro/register/personal": "Personal Info",
    "/pro/register/skills": "Professional Info",
    "/pro/register/payment": "Bank Details",
  };

  useEffect(() => {
    if (authState.isAuth) navigate("/");
    setBreadcrumbState(defaultTrail);

    return () => {
      setBreadcrumbState({});
    };
  }, [ location ]);

  return (
    <Outlet context={[ formData, setFormData ]} ></Outlet>
  );
}
