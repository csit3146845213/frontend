import { Box, Button, Container, TextField, Typography } from "@mui/material";
import { useAuth } from "../../context/AuthContext";
import userLogin from "../../services/userLogin";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

import PasswordForm from "../../components/PasswordForm/PasswordForm";
import AuthContainerForm from "../../components/FormContainer/FormContainer";
import AuthState from "../../types/AuthData";
import { useLoading } from "../../context/LoadingContext";
import { useSnackbar } from "../../context/SnackbarContext";

function Login() {
  const { authState, setAuthState } = useAuth();
  const { isLoading, setIsLoading } = useLoading();
  const { snackbar, setSnackbar } = useSnackbar();
  const [ { email, password }, setForm ] = useState({ email: "", password: "" });
  const navigate = useNavigate();

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setIsLoading(true)
    userLogin(email, password)
      .then((value) => {
        setIsLoading(false)
        setAuthState(value as AuthState)
      })
      .then(() => navigate("/"))
      .catch(() => {
        setSnackbar({ message: "Failed to login", severity: "error" })
      })
  };

  const inputHandler = (e: { currentTarget: { name: any; value: any } }) => {
    const name = e.currentTarget.name;
    const value = e.currentTarget.value;

    setForm((form) => ({ ...form, [ name ]: value }));
  };

  return (
    <AuthContainerForm title={"Login"} onSubmit={handleSubmit}>
      <TextField
        variant="outlined"
        name="email"
        label="Email"
        type="email"
        onChange={inputHandler}
      />
      <PasswordForm onChange={inputHandler}></PasswordForm>
      <Button color="button" variant="contained" type="submit" sx={{ marginTop: "12px" }}>
        Login
      </Button>
    </AuthContainerForm>
  );
}

export default Login;
