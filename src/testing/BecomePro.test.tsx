import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import AuthProvider from "../context/AuthContext";
import LoadingProvider from "../context/LoadingContext";
import SnackbarProvider from "../context/SnackbarContext";
import Login from "../pages/Auth/Login";
import { ThemeProvider } from "@mui/material/styles";
import theme from "../theme";
import BecomeProfessional from "../pages/BecomeProfessional/BecomeProfessional";

describe("Become Professional page", () => {
  test("render without error", () => {
    render(
      <ThemeProvider theme={theme}>
        <MemoryRouter>
          <AuthProvider>
            <LoadingProvider>
              <SnackbarProvider>
                <BecomeProfessional />
              </SnackbarProvider>
            </LoadingProvider>
          </AuthProvider>
        </MemoryRouter>
      </ThemeProvider>
    );
  });
  test('displays "What\'s your Skill?" text', () => {
    render(
        <ThemeProvider theme={theme}>
          <MemoryRouter>
            <AuthProvider>
              <LoadingProvider>
                <SnackbarProvider>
                  <BecomeProfessional />
                </SnackbarProvider>
              </LoadingProvider>
            </AuthProvider>
          </MemoryRouter>
        </ThemeProvider>
      );

    const skillText = screen.getByText("What's your Skill?");
    expect(skillText).toBeInTheDocument();
  });

  test("render all professional images", () => {
    render(
      <ThemeProvider theme={theme}>
        <MemoryRouter>
          <AuthProvider>
            <LoadingProvider>
              <SnackbarProvider>
                <BecomeProfessional />
              </SnackbarProvider>
            </LoadingProvider>
          </AuthProvider>
        </MemoryRouter>
      </ThemeProvider>
    );

    const professionalImages = screen.getAllByRole("img");
    expect(professionalImages.length).toBe(8);
  });
});
