import { render, screen, fireEvent, act, waitFor } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import AuthProvider from "../context/AuthContext";
import LoadingProvider from "../context/LoadingContext";
import SnackbarProvider from "../context/SnackbarContext";
import Login from "../pages/Auth/Login";
import { ThemeProvider } from "@mui/material/styles";
import theme from "../theme";

describe("Login component", () => {
  test("render without error", () => {
    render(
      <ThemeProvider theme={theme}>
        <MemoryRouter>
          <AuthProvider>
            <LoadingProvider>
              <SnackbarProvider>
                <Login />
              </SnackbarProvider>
            </LoadingProvider>
          </AuthProvider>
        </MemoryRouter>
      </ThemeProvider>
    );
  });
  test("renders password input and input value", () => {
    render(
      <ThemeProvider theme={theme}>
        <MemoryRouter>
          <AuthProvider>
            <LoadingProvider>
              <SnackbarProvider>
                <Login />
              </SnackbarProvider>
            </LoadingProvider>
          </AuthProvider>
        </MemoryRouter>
      </ThemeProvider>
    );

    const passwordInput = screen.getByLabelText(/password/i) as HTMLInputElement;

    // Assert the password input is found and check its properties or values
    expect(passwordInput).toBeInTheDocument();
    expect(passwordInput.type).toBe("password");
    expect(passwordInput.value).toBe("");

    
    fireEvent.change(passwordInput, { target: { value: "password123" } });


    // Assert the updated value
    expect(passwordInput.value).toBe("password123");
  });

  test("renders email, password inputs, and login button", () => {
    render(
      <ThemeProvider theme={theme}>
        <MemoryRouter>
          <AuthProvider>
            <LoadingProvider>
              <SnackbarProvider>
                <Login />
              </SnackbarProvider>
            </LoadingProvider>
          </AuthProvider>
        </MemoryRouter>
      </ThemeProvider>
    );

    const emailInput = screen.getByLabelText("Email");
    const passwordInput = screen.getByLabelText(/password/i) as HTMLInputElement;
    const loginButton = screen.getByRole("button", { name: "Login" });

    expect(emailInput).toBeInTheDocument();
    expect(passwordInput).toBeInTheDocument();
    expect(loginButton).toBeInTheDocument();
  });

  test("updates email input value on change", () => {
    render(
      <ThemeProvider theme={theme}>
        <MemoryRouter>
          <AuthProvider>
            <LoadingProvider>
              <SnackbarProvider>
                <Login />
              </SnackbarProvider>
            </LoadingProvider>
          </AuthProvider>
        </MemoryRouter>
      </ThemeProvider>
    );

    const emailInput = screen.getByLabelText("Email") as HTMLInputElement;

    fireEvent.change(emailInput, { target: { value: "test@example.com" } });

    expect(emailInput.value).toBe("test@example.com");
  });
  test("renders email and password input value", () => {
    render(
      <ThemeProvider theme={theme}>
        <MemoryRouter>
          <AuthProvider>
            <LoadingProvider>
              <SnackbarProvider>
                <Login />
              </SnackbarProvider>
            </LoadingProvider>
          </AuthProvider>
        </MemoryRouter>
      </ThemeProvider>
    );

    const passwordInput = screen.getByLabelText(/password/i) as HTMLInputElement;
    const emailInput = screen.getByLabelText("Email") as HTMLInputElement;

    fireEvent.change(emailInput, { target: { value: "test@example.com" } });

    expect(emailInput.value).toBe("test@example.com");
    // Assert the password input is found and check its properties or values
    expect(passwordInput).toBeInTheDocument();
    expect(passwordInput.type).toBe("password");
    expect(passwordInput.value).toBe("");

    
    fireEvent.change(passwordInput, { target: { value: "password123" } });


    // Assert the updated value
    expect(passwordInput.value).toBe("password123");
  });


});
