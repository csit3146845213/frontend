import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import AuthProvider from "../context/AuthContext";
import LoadingProvider from "../context/LoadingContext";
import SnackbarProvider from "../context/SnackbarContext";
import { ThemeProvider } from "@mui/material/styles";
import theme from "../theme";
import Signup from "../components/Signup/Signup";
import ClientSignup from "../pages/Auth/ClientSignup";
import Home from "../pages/Home/Home";


describe("Home page", () => {
    test("render without error", () => {
        render(<ThemeProvider theme={theme}>

            <MemoryRouter>
              <AuthProvider>
                <LoadingProvider>
                  <SnackbarProvider>
                    <Home />
                  </SnackbarProvider>
                </LoadingProvider>
              </AuthProvider>
            </MemoryRouter>
            </ThemeProvider>);
    })
    test("renders the page image", () => {
      const { getByAltText } = render(<ThemeProvider theme={theme}>

        <MemoryRouter>
          <AuthProvider>
            <LoadingProvider>
              <SnackbarProvider>
                <Home />
              </SnackbarProvider>
            </LoadingProvider>
          </AuthProvider>
        </MemoryRouter>
        </ThemeProvider>);
      const image = getByAltText("Half page image");
      expect(image).toBeInTheDocument();
    });
    test("renders the text below the image", () => {
      const { getByText } = render(<ThemeProvider theme={theme}>

        <MemoryRouter>
          <AuthProvider>
            <LoadingProvider>
              <SnackbarProvider>
                <Home />
              </SnackbarProvider>
            </LoadingProvider>
          </AuthProvider>
        </MemoryRouter>
        </ThemeProvider>);
      const text = getByText("Find the perfect professional service for your house");
      expect(text).toBeInTheDocument();
    });

    test("renders the popular professional services", () => {
      const { getByText, getAllByRole } = render(<ThemeProvider theme={theme}>

        <MemoryRouter>
          <AuthProvider>
            <LoadingProvider>
              <SnackbarProvider>
                <Home />
              </SnackbarProvider>
            </LoadingProvider>
          </AuthProvider>
        </MemoryRouter>
        </ThemeProvider>);
      const servicesHeading = getByText("Popular Professional Services");
      const serviceImages = getAllByRole("img");
    
      expect(servicesHeading).toBeInTheDocument();
      expect(serviceImages.length).toBe(6);
    });
})