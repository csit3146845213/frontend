import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import AuthProvider from "../context/AuthContext";
import LoadingProvider from "../context/LoadingContext";
import SnackbarProvider from "../context/SnackbarContext";
import Login from "../pages/Auth/Login";
import { ThemeProvider } from "@mui/material/styles";
import theme from "../theme";
import BecomeProfessional from "../pages/BecomeProfessional/BecomeProfessional";
import Membership from "../pages/Membership/Membership";

describe("Membership page testing", () => {
  test("render without error", () => {
    render(
      <ThemeProvider theme={theme}>
        <MemoryRouter>
          <AuthProvider>
            <LoadingProvider>
              <SnackbarProvider>
                <Membership />
              </SnackbarProvider>
            </LoadingProvider>
          </AuthProvider>
        </MemoryRouter>
      </ThemeProvider>
    );
  });
  test("display body text in membership page", () => {
    render(
      <ThemeProvider theme={theme}>
        <MemoryRouter>
          <AuthProvider>
            <LoadingProvider>
              <SnackbarProvider>
                <Membership />
              </SnackbarProvider>
            </LoadingProvider>
          </AuthProvider>
        </MemoryRouter>
      </ThemeProvider>
    );

    const textElement = screen.getByText(
      /Get the most benefit out of TradieLog membership/i
    );
    expect(textElement).toBeInTheDocument();
  });

  test("renders title and subtitle within NotSubscribedContainer component", () => {
    const title = "TradieLog Membership";
    const subtitle = "Provide the best services with extra perks";
    render(
      <ThemeProvider theme={theme}>
        <MemoryRouter>
          <AuthProvider>
            <LoadingProvider>
              <SnackbarProvider>
                <Membership />
              </SnackbarProvider>
            </LoadingProvider>
          </AuthProvider>
        </MemoryRouter>
      </ThemeProvider>
    );

    const titleElement = screen.getByText(title);
    const subtitleElement = screen.getByText(subtitle);

    expect(titleElement).toBeInTheDocument();
    expect(subtitleElement).toBeInTheDocument();
  });

  test("render membership image", () => {
    render(
      <ThemeProvider theme={theme}>
        <MemoryRouter>
          <AuthProvider>
            <LoadingProvider>
              <SnackbarProvider>
                <Membership />
              </SnackbarProvider>
            </LoadingProvider>
          </AuthProvider>
        </MemoryRouter>
      </ThemeProvider>
    );

    const membershipImage = screen.getAllByRole("img");
    expect(membershipImage.length).toBe(1);
  });
});
