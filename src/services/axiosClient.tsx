import axios from "axios";

import { Config, baseUrl } from "../data/Config";
function fetchClient() {
  const defaultOptions = {
    baseURL: baseUrl,
    headers: {
      'Content-Type': "application/json",
    }
  }
  let instance = axios.create(defaultOptions)
  instance.interceptors.request.use(function (config) {
    const token = localStorage.getItem('token');
    config.headers.Authorization = token ? `Bearer ${token}` : '';
    return config;
  });

  return instance
}

export default fetchClient;
