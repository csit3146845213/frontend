import axios from "axios";
import errorHandler from "../errorHandler";
import fetchClient from "../axiosClient";
import { GenericForm } from "../../types/GenericForm";

async function proRespondRequest(respondData: GenericForm): Promise<any> {
    try {
        const { data } = await fetchClient()({
            url: `service/respond-request`,
            method: "POST",
            data: respondData
        });

        return data;
    } catch (error) {
        errorHandler(error);
    }
}

export default proRespondRequest;
