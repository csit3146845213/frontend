import axios from "axios";
import errorHandler from "../errorHandler";
import fetchClient from "../axiosClient";
import { GenericForm } from "../../types/GenericForm";

async function finishService(serviceId: number) {
    try {
        const { data } = await fetchClient()({
            url: `service/mark-done`,
            method: "PUT",
            data: { service_id: serviceId }
        });

        return data;
    } catch (error) {
        errorHandler(error);
    }
}

export default finishService;
