import errorHandler from "../errorHandler";
import fetchClient from "../axiosClient";

async function cancelRequest(serviceId: number) {
  try {
    const { data } = await fetchClient()({
      url: `service/cancel`,
      method: "PUT",
      data: { service_id: serviceId }
    });

    return data;
  } catch (error) {
    errorHandler(error);
  }
}

export default cancelRequest;
