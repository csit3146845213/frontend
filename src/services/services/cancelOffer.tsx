import axios from "axios";
import errorHandler from "../errorHandler";
import fetchClient from "../axiosClient";

async function cancelOffer(serviceId: number) {
  try {
    const { data } = await fetchClient()({
      url: `service/cancel-offer`,
      method: "DELETE",
      data: { service_id: serviceId }
    });

    return data;
  } catch (error) {
    errorHandler(error);
  }
}

export default cancelOffer;
