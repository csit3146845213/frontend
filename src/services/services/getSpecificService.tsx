import axios from "axios";
import errorHandler from "../errorHandler";
import fetchClient from "../axiosClient";

async function getSpecificService(serviceId: string) {
  try {
    const { data } = await fetchClient()({
      url: `service/${serviceId}`,
      method: "GET",
    });

    return data.serviceRequest;
  } catch (error) {
    errorHandler(error);
  }
}

export default getSpecificService;
