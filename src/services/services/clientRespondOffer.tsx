import axios from "axios";
import errorHandler from "../errorHandler";
import fetchClient from "../axiosClient";
import { GenericForm } from "../../types/GenericForm";

async function clientRespondOffer(serviceId: number, professionalId: number, accept: boolean) {
    try {
        const { data } = await fetchClient()({
            url: `service/respond-offer`,
            method: "PUT",
            data: { service_id: serviceId, professional_id: professionalId, accept: accept }
        });

        return data;
    } catch (error) {
        errorHandler(error);
    }
}

export default clientRespondOffer;
