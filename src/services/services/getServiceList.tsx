import axios from "axios";
import errorHandler from "../errorHandler";
import fetchClient from "../axiosClient";

async function getServiceList(serviceType: string) {
  try {
    const { data } = await fetchClient()({
      url: `service/list`,
      method: "GET",
      params: { service_status: serviceType }
    });

    return data.serviceRequests;
  } catch (error) {
    errorHandler(error);
  }
}

export default getServiceList;
