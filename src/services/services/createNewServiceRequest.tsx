import axios from "axios";
import errorHandler from "../errorHandler";
import fetchClient from "../axiosClient";
import { GenericForm } from "../../types/GenericForm";

async function createNewServiceRequest(requestInfo: GenericForm) {
  try {
    const { data } = await fetchClient()({
      data: requestInfo,
      url: "service/create",
      method: "POST",
    });

    return data;
  } catch (error) {
    errorHandler(error);
  }
}

export default createNewServiceRequest;
