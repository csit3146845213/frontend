import axios from "axios";
import errorHandler from "../errorHandler";
import fetchClient from "../axiosClient";

async function getNearbyServices(radius: number) {
  try {
    const { data } = await fetchClient()({
      url: "service/nearby",
      method: "GET",
      params: { radius: radius }
    });

    return data.serviceRequests;
  } catch (error) {
    errorHandler(error);
  }
}

export default getNearbyServices;
