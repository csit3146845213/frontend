import axios from "axios";
import errorHandler from "../errorHandler"
import { GenericForm } from "../../types/GenericForm";
import fetchClient from "../axiosClient";

async function clientUpdateProfile(updateProfile: GenericForm) {
  try {
    const { data } = await fetchClient()({
      data: updateProfile,
      method: "POST",
      url: "auth/update-profile",
    });

    return data
  } catch (error) {
    errorHandler(error)
  }
}

export default clientUpdateProfile
