import axios from "axios";
import errorHandler from "../errorHandler"
import authenticatedClient from "../axiosClient";

async function getCard() {
    try {
        const { data } = await authenticatedClient()({
            method: "GET",
            url: "payment/cards",
        });

        return data
    } catch (error) {
        errorHandler(error)
    }
}

export default getCard
