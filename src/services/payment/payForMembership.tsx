// /payment/register-card
import axios from "axios";
import errorHandler from "../errorHandler"
import authenticatedClient from "../axiosClient";
import { GenericForm } from "../../types/GenericForm";

async function payForMembership(arg: GenericForm) {
    try {
        const { data } = await authenticatedClient()({
            method: "POST",
            url: "membership/register",
            data: arg
        });

        return data
    } catch (error) {
        errorHandler(error)
    }
}

export default payForMembership
