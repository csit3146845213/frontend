// /payment/register-card
import axios from "axios";
import errorHandler from "../errorHandler"
import authenticatedClient from "../axiosClient";
import { GenericForm } from "../../types/GenericForm";

async function addCard(arg: GenericForm) {
    try {
        arg.card_num = parseInt(arg.card_num)
        arg.ccv = parseInt(arg.ccv)
        const { data } = await authenticatedClient()({
            method: "POST",
            url: "payment/register-card",
            data: {
                ...arg,
                first_name: arg.card_name,
                last_name: "‎"
            }
        });

        return data
    } catch (error) {
        errorHandler(error)
    }
}

export default addCard
