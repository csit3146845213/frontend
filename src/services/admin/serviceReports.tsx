// /payment/register-card
import axios from "axios";
import errorHandler from "../errorHandler"
import authenticatedClient from "../axiosClient";
import { GenericForm } from "../../types/GenericForm";

async function serviceReports(mindate: string, maxdate: string) {
    try {
        const { data } = await authenticatedClient()({
            method: "GET",
            url: "reports/services",
            params: {
                datemin: mindate,
                datemax: maxdate
            }
        });

        return data
    } catch (error) {
        errorHandler(error)
    }
}

export default serviceReports
