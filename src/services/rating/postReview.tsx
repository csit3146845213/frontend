import axios from "axios";
import errorHandler from "../errorHandler";
import fetchClient from "../axiosClient";
import { GenericForm } from "../../types/GenericForm";

async function postReview(ratingInfo: GenericForm) {
  try {
    const { data } = await fetchClient()({
      data: ratingInfo,
      url: "review",
      method: "POST",
    });

    return data;
  } catch (error) {
    errorHandler(error);
  }
}

export default postReview;
