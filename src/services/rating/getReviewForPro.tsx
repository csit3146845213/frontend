
import axios from "axios";
import errorHandler from "../errorHandler";
import fetchClient from "../axiosClient";

async function getReviewForPro(professionalId: number) {
  try {
    const { data } = await fetchClient()({
      url: "review",
      method: "GET",
      params: { pro_id: professionalId }
    });

    return data;
  } catch (error) {
    errorHandler(error);
  }
}

export default getReviewForPro;
