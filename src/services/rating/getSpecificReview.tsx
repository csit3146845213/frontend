
import axios from "axios";
import errorHandler from "../errorHandler";
import fetchClient from "../axiosClient";

async function getSpecificReview(professionalId: number, serviceId: number) {
  try {
    const { data } = await fetchClient()({
      url: "review",
      method: "GET",
      params: { pro_id: professionalId, service_id: serviceId }
    });

    return data;
  } catch (error) {
    errorHandler(error);
  }
}

export default getSpecificReview;
