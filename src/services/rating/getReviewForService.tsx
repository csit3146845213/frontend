
import axios from "axios";
import errorHandler from "../errorHandler";
import fetchClient from "../axiosClient";

async function getReviewForService(serviceId: number) {
  try {
    const { data } = await fetchClient()({
      url: "review",
      method: "GET",
      params: { service_id: serviceId }
    });

    return data;
  } catch (error) {
    errorHandler(error);
  }
}

export default getReviewForService;
