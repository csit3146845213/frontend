import axios from "axios";
import errorHandler from "../errorHandler";
import AuthState from "../../types/AuthData";
import { GenericForm } from "../../types/GenericForm";
import fetchClient from "../axiosClient";

async function clientRegister(signUpForm: GenericForm) {
  try {
    const { data } = await axios({
      data: signUpForm,
      method: "POST",
      url: "auth/register",
    });

    const headers = { Authorization: `Bearer ${data.accessToken}` };

    const loggedIn: AuthState = {
      headers: headers,
      isAuth: true,
      userData: data.user,
      userType: data.user.user_type
    };
    console.log(loggedIn)
    localStorage.setItem("loggedUser", JSON.stringify(loggedIn));
    localStorage.setItem("token", data.accessToken)

    return loggedIn;
  } catch (error) {
    errorHandler(error);
  }
}

export default clientRegister;
