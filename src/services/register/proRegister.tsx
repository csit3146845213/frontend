import axios from "axios";
import errorHandler from "../errorHandler";
import AuthState from "../../types/AuthData";
import { GenericForm } from "../../types/GenericForm";

async function proRegister(signUpForm: GenericForm) {
  signUpForm.skills = [ ...signUpForm.skills ]
  try {
    signUpForm.card_num = parseInt(signUpForm.card_num)
    signUpForm.ccv = parseInt(signUpForm.ccv)
    const { data } = await axios({
      data: signUpForm,
      method: "POST",
      url: "auth/register-prof",
    });

    const headers = { Authorization: `Bearer ${data.accessToken}` };
    const loggedIn: AuthState = {
      headers: headers,
      isAuth: true,
      userData: data.user,
      userType: data.user.user_type
    };

    localStorage.setItem("loggedUser", JSON.stringify(loggedIn));
    localStorage.setItem("token", data.accessToken);

    return loggedIn;
  } catch (error) {
    errorHandler(error);
  }
}

export default proRegister;
