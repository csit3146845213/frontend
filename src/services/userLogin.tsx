import axios from "axios";
import errorHandler from "./errorHandler";
import fetchClient from "./axiosClient";

async function userLogin(email: string, password: string) {
  try {
    const { data } = await axios({
      data: { email, password },
      method: "POST",
      url: "auth/login",
    });

    const headers = { Authorization: `Bearer ${data.accessToken}` };
    const loggedIn = { headers, isAuth: true, userData: data.user, userType: data.user.user_type };

    localStorage.setItem("loggedUser", JSON.stringify(loggedIn));
    localStorage.setItem("token", data.accessToken);

    return loggedIn;
  } catch (error) {
    errorHandler(error);
  }
}

export default userLogin;
