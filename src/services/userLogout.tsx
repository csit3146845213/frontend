import AuthState from "../types/AuthData";

function userLogout(): AuthState {
  localStorage.removeItem("loggedUser");
  localStorage.removeItem("token");

  return AuthState.createFromString("")
}

export default userLogout;
