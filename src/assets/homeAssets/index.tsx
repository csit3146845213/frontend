import fenceInstallation from './fenceInstalation.png';
import homepageBig from './homepageBig.png';
import lawnMowing from './lawnMowing.png';
import ovenRepair from './ovenRepairs.png';
import roofCleaning from './roofCleaning.png';
import treeRemoval from './treeRemoval.png';

export interface HomeAssets {
    fenceInstallation: string;
    homepageBig: string;
    lawnMowing: string;
    ovenRepair: string;
    roofCleaning: string;
    treeRemoval: string;
  }
  
  const assets: HomeAssets = {
    fenceInstallation,
    homepageBig,
    lawnMowing,
    ovenRepair,
    roofCleaning,
    treeRemoval 
  };
  
  export default assets;