import membership1 from './Membership1.jpg';
import membership2 from './Membership2.jpg';

export interface MembershipAssets{
    membership1: string,
    membership2: string

}

const assets: MembershipAssets = {
    membership1,
    membership2
}

export default assets;