import prof1 from './prof1.png';
import prof2 from './prof2.png';
import prof3 from './prof3.png';
import prof4 from './prof4.png';
import prof5 from './prof5.png';
import prof6 from './prof6.png';
import prof7 from './prof7.png';
import blue from './blue.png';

export interface BecomeProAssets {
    prof1: string;
    prof2: string;
    prof3: string;
    prof4: string;
    prof5: string;
    prof6: string;
    prof7: string;
    blue: string;
}

const assets: BecomeProAssets = {
    prof1,
    prof2,
    prof3,
    prof4,
    prof5,
    prof6,
    prof7,
    blue


};

export default assets;
