
import service1 from './services1.png'
import service2 from './services2.png'
import service3 from './services3.png'
import service4 from './services4.png'
import service5 from './services5.png'

const serviceAssets: {[key:string] : string }= {
    "Fence Installation": service1,
    "Roof Cleaning": service2,
    "Tree Removal": service3,
    "Lawn Mowing": service4,
    "Oven Repairs": service5,
}
export default serviceAssets
