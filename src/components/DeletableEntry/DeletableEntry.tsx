import {
  FormControl,
  IconButton,
  InputAdornment,
  OutlinedInput,
} from "@mui/material";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";
import { MouseEventHandler } from "react";

interface Props {
  value: string;
  onClickDelete: MouseEventHandler<HTMLButtonElement>;
}

export default function DeletableEntry({ value, onClickDelete }: Props) {
  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    event.preventDefault();
  };

  return (
    <FormControl variant="outlined" sx={{ marginTop: "12px" }}>
      <OutlinedInput
        sx={{ color: "primary.main", textAlign: "left" }}
        readOnly

        value={value}
        required={true}
        type={"text"}
        endAdornment={
          <InputAdornment position="end">
            <IconButton
              value={value}
              onClick={onClickDelete}
              onMouseDown={handleMouseDownPassword}
            >
              {<HighlightOffIcon />}
            </IconButton>
          </InputAdornment>
        }
      ></OutlinedInput>
    </FormControl>
  );
}
