import { OutlinedInputProps, TextField, Typography } from "@mui/material";
import FormContainer from "../FormContainer/FormContainer";
import { ChangeEvent, Dispatch, FormEvent, FormEventHandler, ReactNode, SetStateAction, useState } from "react";
import formatNumberInput from "../../hooks/formatNumberInput";
import { GenericForm } from "../../types/GenericForm";
import Address from "../Address/Address";
import validator from "validator";

interface Props {
  onSubmit?: FormEventHandler<HTMLFormElement>;
  readonly?: boolean;
  title: string;
  requestInfo: GenericForm
  setRequestInfo: Dispatch<SetStateAction<GenericForm>>
  children?: ReactNode;
}

export default function RequestDetails({
  onSubmit,
  readonly,
  requestInfo,
  setRequestInfo,
  children,
  title,
}: Props) {
  const inputProps: Partial<OutlinedInputProps> = {
    readOnly: readonly,
  };
  const inputLabelProps = {
    shrink: true
  }
  const [ formErrors, setFormErrors ] = useState({} as GenericForm);
  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setRequestInfo({ ...requestInfo, [ event.target.name ]: event.target.value });
  };

  const handleNumberChange = (event: ChangeEvent<HTMLInputElement>) => {
    setRequestInfo((prev: any) => {
      const value = formatNumberInput(
        event.target.value,
        prev[ event.target.name ]
      );
      return {
        ...requestInfo,
        [ event.target.name ]: value,
      };
    });
  };

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    if (onSubmit && !fieldValidators(event)) {
      onSubmit(event)
    }
  }

  const appendFormErrors = (key: string, errorMessage: string) => {
    setFormErrors((prev) => {
      return {
        ...prev,
        [ key ]: errorMessage
      }
    })
  }

  const fieldValidators = (event: FormEvent<HTMLFormElement>) => {
    setFormErrors({})
    if (!validator.isPostalCode((requestInfo.postcode).toString(), "any")) {
      appendFormErrors("postcode", "Not a valid postal code")
    }

    if (requestInfo.home_number !== undefined && !validator.isMobilePhone((requestInfo.home_number).toString())) {
      appendFormErrors("home_number", "Not a valid number")
    }

    if (!validator.isMobilePhone((requestInfo.mobile).toString())) {
      appendFormErrors("mobile", "Not a valid phone number")
    }

    if (!validator.isCurrency(requestInfo.expected_price.toString())) {
      appendFormErrors("price", "Not a valid amount")
    } else {
      setRequestInfo((prev) => {
        return {
          ...prev,
          price: Number(prev.price)
        }
      })
    }

    return Object.keys(formErrors).length > 0;
  }

  return (
    <FormContainer title={title} onSubmit={handleSubmit}>
      <TextField
        sx={{ mt: "12px" }}
        required
        variant="outlined"
        name="service_name"
        label="Service"
        value={requestInfo.service_name ?? ""}
        onChange={handleChange}
        disabled
        InputLabelProps={inputLabelProps}
        InputProps={{
          readOnly: true
        }}
      />
      <TextField
        sx={{ mt: "12px" }}
        required
        variant="outlined"
        name="service_request_date"
        onChange={handleChange}
        type="datetime-local"
        value={requestInfo.service_request_date ?? ""}
        disabled
        InputProps={{
          readOnly: true
        }}
      />
      <TextField
        sx={{ mt: "12px" }}
        required
        variant="outlined"
        value={requestInfo.expected_duration ?? ""}
        onChange={handleChange}
        name="expected_duration"
        label="Expected duration"
        InputLabelProps={inputLabelProps}
        InputProps={inputProps}
      />
      <TextField
        sx={{ mt: "12px" }}
        required
        variant="outlined"
        name="expected_price"
        error={(formErrors[ "price" ] ?? "").length > 0}
        helperText={formErrors[ "price" ] ?? ""}
        value={requestInfo.expected_price ?? ""}
        InputLabelProps={inputLabelProps}
        label="Expected price"
        onChange={handleChange}
        InputProps={{
          ...inputProps,
          startAdornment: "$",
        }}
      />
      <Address readOnly={readonly} formData={requestInfo} setFormData={setRequestInfo}></Address>
      <TextField
        sx={{ mt: "12px" }}
        variant="outlined"
        value={requestInfo.home}
        name="phone"
        error={(formErrors[ "home_number" ] ?? "").length > 0}
        helperText={formErrors[ "home_number" ] ?? ""}
        label="Home Phone"
        InputLabelProps={inputLabelProps}
        onChange={handleNumberChange}
        InputProps={inputProps}
      />
      <TextField
        required
        sx={{ mt: "12px" }}
        variant="outlined"
        name="mobile"
        value={requestInfo.mobile ?? ""}
        label="Mobile"
        error={(formErrors[ "mobile" ] ?? "").length > 0}
        helperText={formErrors[ "mobile" ] ?? ""}
        InputLabelProps={inputLabelProps}
        onChange={handleNumberChange}
        InputProps={inputProps}
      />
      <TextField
        sx={{ mt: "12px" }}
        variant="outlined"
        name="description"
        value={requestInfo.description ?? ""}
        label="Additional information"
        onChange={handleChange}
        InputLabelProps={inputLabelProps}
        multiline
        rows={4}
        InputProps={inputProps}
      />
      {children}
    </FormContainer>
  );
}
