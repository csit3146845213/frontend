import { Navigate, Route } from "react-router-dom";
import { useAuth } from "../../context/AuthContext";
import { UserTypes } from "../../types/AuthData";

interface Props {
  userType?: UserTypes;
  component: JSX.Element;
}

export default function Private({ userType, component }: Props) {
  const { authState, setAuthState } = useAuth();

  const userIsOfRightType =
    authState.userType === userType || !userType;
  return authState.isAuth && userIsOfRightType ? (
    component
  ) : (
    <Navigate to={"/login"}></Navigate>
  );
}
