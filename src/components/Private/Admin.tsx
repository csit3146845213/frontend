import { Navigate, Route } from "react-router-dom";
import { useAuth } from "../../context/AuthContext";
import Private from "./Private";

interface Props {
  children: JSX.Element;
}

export default function Pro({ children }: Props) {
  return <Private userType="Admin" component={children}></Private>;
}
