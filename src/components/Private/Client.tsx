import { Navigate, Route } from "react-router-dom";
import { useAuth } from "../../context/AuthContext";
import Private from "./Private";

interface Props {
  children: JSX.Element;
}

export default function Client({ children }: Props) {
  return <Private userType="Client" component={children}></Private>;
}
