import { TextField } from "@mui/material";
import AddressAutocomplete, { AddressAutocompleteValue } from "mui-address-autocomplete";
import { ChangeEvent, Dispatch, SetStateAction, useEffect, useState } from "react";
import { GenericForm } from "../../types/GenericForm";
import { Secret } from "../../data/Secret";
import formatNumberInput from "../../hooks/formatNumberInput";

interface Props {
  formData: GenericForm
  setFormData: Dispatch<SetStateAction<GenericForm>>
  readOnly?: boolean
}

export default function Address({ formData, setFormData, readOnly = false }: Props) {
  const [ address, setAddress ] = useState<AddressAutocompleteValue | null>({
    structured_formatting: {
      main_text: "",
      main_text_matched_substrings: [],
      secondary_text: ""
    },
    description: "",
    components: {}
  })

  useEffect(() => {
    setAddress({
      structured_formatting: {
        main_text: formData.street_address ?? "",
        main_text_matched_substrings: [],
        secondary_text: formData.street_address ?? ""
      },
      description: formData.street_address ?? "",
      components: {}
    })
  }, [ formData.street_address ])

  const handleNumberChange = (event: ChangeEvent<HTMLInputElement>) => {
    setFormData((prev: any) => {
      const value = formatNumberInput(
        event.target.value,
        prev[ event.target.name ]
      );
      return {
        ...formData,
        [ event.target.name ]: value,
      };
    });
  };

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setFormData({ ...formData, [ event.target.name ]: event.target.value });
  };

  return (
    <>
      <AddressAutocomplete
        sx={{ mt: "12px" }}
        apiKey={Secret.PLACE_API_KEY}
        label="Address"
        readOnly={readOnly}
        getOptionLabel={(option) => option.description || ""}
        onChange={(_, value) => {
          setAddress(value);
          setFormData((prev) => {
            let postcode = "";
            if (value?.components.postal_code !== undefined) {
              postcode = value?.components.postal_code[ 0 ].short_name
            }
            const country = value?.components.country[ 0 ].long_name ?? ""
            const address = value?.description ?? ""
            return {
              ...prev,
              postcode: postcode,
              country: country,
              street_address: address
            }
          })
        }}
        value={address} />
      <TextField sx={{ mt: "12px" }} onChange={handleNumberChange} required name="postcode" value={formData.postcode ?? ""} label="Postal Code"></TextField>
      <TextField sx={{ mt: "12px" }} onChange={handleChange} required name="country" value={formData.country ?? ""} label="Country"></TextField>
    </>
  )
}
