import ServiceCard from "./ServiceCard";
import { CardProps } from "../ServiceGrid/ServiceGrid";
import { Button, Grid } from "@mui/material";
import { useAuth } from "../../context/AuthContext";
import { useNavigate } from "react-router-dom";
import serviceAssets from "../../assets/serviceAssets/serviceAssets";
import useHttpRequest from "../../hooks/useHttpRequest";
import getSpecificReview from "../../services/rating/getSpecificReview";
import { useSnackbar } from "../../context/SnackbarContext";
import { GenericForm } from "../../types/GenericForm";
import { useEffect, useState } from "react";

export default function ProFinishedServiceCard({ cardData, setCardList }: CardProps) {
  let newDate = new Date(cardData.service_request_date);
  const { authState, setAuthState } = useAuth();
  const { snackbar, setSnackbar } = useSnackbar()
  const [ buttonDisabled, setButtonDisabled ] = useState(false)
  const [ reviewData, setReviewData ] = useState<null | GenericForm>(null);
  const navigate = useNavigate()

  const onClick = useHttpRequest(() => getSpecificReview(authState.userData.user_id, cardData.service_id), value => {
    const newData = (value ?? { reviews: [] }).reviews as GenericForm[];
    console.log(value)
    setReviewData({ ...(newData[ 0 ] ?? {}) })
  }, "", true)

  useEffect(() => {
    // idc anymore
    if (Object.keys(reviewData ?? {}).length > 0) {
      navigate(`/pro/rating`, { state: { service_id: cardData.service_id, pro_id: cardData.serviceOffers[ 0 ].professional_id } })
    } else {
      if (reviewData !== null) {
        setSnackbar({ message: "Client has not posted review yet", severity: "warning" })
        setButtonDisabled(true)
        setReviewData(null)
      }
    }
  }, [ reviewData ])

  return (
    <Grid item xs={"auto"} key={cardData.service_id}>
      <ServiceCard image={serviceAssets[ cardData.service_name ]} title={cardData.service_name} price={`${cardData.serviceOffers[ 0 ].offer_price}`} values={{
        "Booked Date": newDate.toLocaleDateString(),
        "Location": cardData.street_address,
        "Status": cardData.service_status
      }}>
        <Button color="button" disabled={buttonDisabled} onClick={onClick} sx={{ my: "12px" }} variant="contained">
          View Client Review
        </Button>
      </ServiceCard>
    </Grid>
  )
}
