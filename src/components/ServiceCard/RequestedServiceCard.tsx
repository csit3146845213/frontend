import ServiceCard from "./ServiceCard";
import { CardProps } from "../ServiceGrid/ServiceGrid";
import { Button, Grid } from "@mui/material";
import cancelRequest from "../../services/services/cancelRequest";
import serviceAssets from "../../assets/serviceAssets/serviceAssets";
import useHttpRequest from "../../hooks/useHttpRequest";

export default function RequestedServiceCard({ cardData, setCardList }: CardProps) {
  let newDate = new Date(cardData.service_request_date);
  const deleteSelf = () => {
    setCardList((prev) => {
      return prev.filter(prev => prev.service_id != cardData.service_id)
    })
  }

  // This function took 6 hours of my life
  // + 2
  const onClick = useHttpRequest(() => cancelRequest(cardData.service_id), deleteSelf, "Succesfully cancelled request")

  return (
    <Grid item xs={"auto"} key={`req-${cardData.service_id}`}>
      <ServiceCard image={serviceAssets[ cardData.service_name ]} title={cardData.service_name} values={{
        "Booked Date": newDate.toLocaleDateString(),
        "Location": cardData.street_address,
        "Expected Price": `$${cardData.expected_price}`,
        "Status": cardData.service_status
      }}>
        <Button color="danger" onClick={onClick} sx={{ my: "12px" }} variant="contained">
          Cancel Request
        </Button>
      </ServiceCard>
    </Grid>
  )
}
