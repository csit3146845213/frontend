import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  SxProps,
  Theme,
  Typography,
} from "@mui/material";
import { ReactNode } from "react";
interface Props {
  title: string;
  values: { [ key: string ]: string };
  children?: ReactNode;
  price?: string;
  isVisible?: boolean;
  image: string;
  companyName?: string;
}

export const cardVisibleSx = {
  visibility: "visible",
  opacity: 1,
  transition: "opacity 0.3s linear"
}
export const cardNotVisibleSx = {
  visibility: "hidden",
  opacity: 0,
  transition: "visibility 0s 0.3s, opacity 0.3s linear"
}

export default function ServiceCard({
  title,
  companyName,
  values,
  price,
  children,
  image,
  isVisible = true
}: Props) {
  values = values ?? {};
  companyName = companyName ?? "";
  const keys = Object.keys(values !== undefined ? values : []);
  const isVisibleSx = isVisible ? cardVisibleSx : cardNotVisibleSx
  return (
    <Card
      sx={{
        minWidth: 250,
        flexGrow: "1",
        maxWidth: 250,
        border: "solid 1px black",
        ...isVisibleSx
      }}
    >
      <CardMedia image={image} component={"img"} height={"150"} />
      <CardContent
        sx={{
          "&:last-child": {
            paddingBottom: 0,
          },
        }}
      >
        <Typography
          gutterBottom
          variant="h6"
          textAlign={"left"}
          component="div"
        >
          {title}
        </Typography>
        {companyName?.length > 0 && (
          <Typography sx={{ mb: "12px" }} textAlign={"left"} component="div">
            by {companyName}
            <br />
            {price}
          </Typography>
        )}

        {keys.length > 0 &&
          [ ...keys ].map((key) => {
            return (
              <Typography
                key={key}
                variant="body2"
                noWrap
                textAlign={"justify"}
              >
                {`${key}: ${values[ key ]}`}
              </Typography>
            );
          })}

        <CardActions>
          <Box display={"flex"} flexDirection={"column"} width={"100%"}>
            {children}
          </Box>
        </CardActions>
      </CardContent>
    </Card>
  );
}
