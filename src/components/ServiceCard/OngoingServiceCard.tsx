import ServiceCard from "./ServiceCard";
import { CardProps } from "../ServiceGrid/ServiceGrid";
import { Button, Grid } from "@mui/material";
import serviceAssets from "../../assets/serviceAssets/serviceAssets";

export default function OngoingServiceCard({ cardData, setCardList }: CardProps) {
  let newDate = new Date(cardData.service_request_date);
  let price = 0
  if ((cardData.serviceOffers ?? []).length > 0) {
    price = cardData.serviceOffers[ 0 ].offer_price
  }
  return (
    <Grid item xs={"auto"} key={cardData.service_id}>
      <ServiceCard image={serviceAssets[ cardData.service_name ]} title={cardData.service_name} companyName={`${cardData.serviceOffers[ 0 ].first_name} ${(cardData.serviceOffers[ 0 ].last_name) ? cardData.serviceOffers[ 0 ].last_name : ""}`} price={`$${price}`} values={{
        "Booked Date": newDate.toLocaleDateString(),
        "Location": cardData.street_address,
        "Status": cardData.service_status
      }}>
      </ServiceCard>
    </Grid>
  )
}
