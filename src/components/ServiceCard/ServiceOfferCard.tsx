import ServiceCard from "./ServiceCard";
import { CardProps } from "../ServiceGrid/ServiceGrid";
import { Button, Grid } from "@mui/material";
import { GenericForm } from "../../types/GenericForm";
import React from "react";
import serviceAssets from "../../assets/serviceAssets/serviceAssets";
import useHttpRequest from "../../hooks/useHttpRequest";
import clientRespondOffer from "../../services/services/clientRespondOffer";
import { useNavigate } from "react-router-dom";

export default function ServiceOfferCard({ cardData, setCardList }: CardProps) {
  let newDate = new Date(cardData.service_request_date);
  const navigate = useNavigate()
  const deleteSpecificOffer = (professionalId: number) => {
    return () => {
      let currentServiceOffers: GenericForm[] = cardData.serviceOffers ?? []
      const newService = currentServiceOffers.filter(offer => offer.professional_id != professionalId)
      setCardList((prev) => {
        let newPrev = [ ...prev ]
        for (let i = 0; i < newPrev.length; ++i) {
          if (newPrev[ i ].service_id != cardData.service_id) continue
          newPrev[ i ].serviceOffers = newService
          break
        }
        // I hate react
        return prev
      })
    }
  }

  const declineOffer = (professionalId: number) => {
    return useHttpRequest(() => clientRespondOffer(cardData.service_id, professionalId, false), deleteSpecificOffer(professionalId), "Succesfully accepted offer")
  }

  const goToDetails = () => {
    navigate("/client/offer", { state: { cardData: cardData } })
  }
  // One of the react codes of all time
  // Extracts all serviceOffers from cardData and makes a child for each offer
  return (
    <React.Fragment key={cardData.service_id}>
      {(cardData.serviceOffers ?? []).map((offer: GenericForm) => {
        return <Grid item xs={"auto"} key={offer.professional_id}>
          <ServiceCard image={serviceAssets[ cardData.service_name ]} price={`$${offer.offer_price}`} companyName={`${offer.first_name} ${offer.last_name ? offer.last_name : ""}`} title={cardData.service_name} values={{
            "Booked Date": newDate.toLocaleDateString(),
            "Location": cardData.street_address,
            "Status": cardData.service_status
          }}>
            <Button onClick={goToDetails} sx={{ mb: "12px" }} variant="contained">
              View Details
            </Button>
            <Button onClick={declineOffer(offer.professional_id)} sx={{ mb: "12px" }} variant="contained">
              Decline Offer
            </Button>
          </ServiceCard>
        </Grid>
      })}
    </React.Fragment >
  )

}
