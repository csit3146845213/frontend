import ServiceCard from "./ServiceCard";
import { CardProps } from "../ServiceGrid/ServiceGrid";
import { Button, Grid } from "@mui/material";
import cancelOffer from "../../services/services/cancelOffer";
import serviceAssets from "../../assets/serviceAssets/serviceAssets";
import useHttpRequest from "../../hooks/useHttpRequest";
import cancelRequest from "../../services/services/cancelRequest";

export default function OfferCard({ cardData, setCardList }: CardProps) {

  let newDate = new Date(cardData.service_request_date);
  const deleteSelf = () => {
    setCardList((prev) => {
      return prev.filter(prev => prev.service_id != cardData.service_id)
    })
  }

  const handleClick = useHttpRequest(() => cancelOffer(cardData.service_id), deleteSelf, "Succesfully cancelled offer")

  return (
    <Grid item xs={"auto"} key={cardData.service_id}>
      <ServiceCard image={serviceAssets[ cardData.service_name ]} title={cardData.service_name} values={{
        "Booked Date": newDate.toLocaleDateString(),
        "Location": cardData.street_address,
        "Status": cardData.service_status
      }}>
        <Button onClick={handleClick} variant="contained">Cancel Offer</Button>
      </ServiceCard>
    </Grid>
  )
}
