import ServiceCard from "./ServiceCard";
import { CardProps } from "../ServiceGrid/ServiceGrid";
import { Button, Grid } from "@mui/material";
import serviceAssets from "../../assets/serviceAssets/serviceAssets";
import useHttpRequest from "../../hooks/useHttpRequest";
import finishService from "../../services/services/finishService";

export default function ProOngoingServiceCard({ cardData, setCardList }: CardProps) {
  let newDate = new Date(cardData.service_request_date);
  let price = 0;

  if ((cardData.serviceOffers ?? []).length > 0) {
    price = cardData.serviceOffers[ 0 ].offer_price
  }

  const deleteSelf = () => {
    setCardList((prev) => {
      return prev.filter(prev => prev.service_id != cardData.service_id)
    })
  }

  const onClick = useHttpRequest(() => finishService(cardData.service_id), deleteSelf, "Succesfully finished service")
  return (
    <Grid item xs={"auto"} key={cardData.service_id}>
      <ServiceCard image={serviceAssets[ cardData.service_name ]} title={cardData.service_name} price={`$${price}`} values={{
        "Booked Date": newDate.toLocaleDateString(),
        "Location": cardData.street_address,
        "Status": cardData.service_status
      }}>
        <Button color="button" sx={{ my: "12px" }} onClick={onClick} variant="contained">Finish Service</Button>
      </ServiceCard>
    </Grid>
  )
}
