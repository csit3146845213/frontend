import ServiceCard from "./ServiceCard";
import { CardProps } from "../ServiceGrid/ServiceGrid";
import { Button, Grid } from "@mui/material";
import { useAuth } from "../../context/AuthContext";
import { useNavigate } from "react-router-dom";
import serviceAssets from "../../assets/serviceAssets/serviceAssets";
import useHttpRequest from "../../hooks/useHttpRequest";
import getSpecificReview from "../../services/rating/getSpecificReview";
import { GenericForm } from "../../types/GenericForm";
import { useEffect, useState } from "react";
import getReviewForService from "../../services/rating/getReviewForService";
import { useSnackbar } from "../../context/SnackbarContext";

export default function ClientFinishedServiceCard({ cardData, setCardList }: CardProps) {
  let newDate = new Date(cardData.service_request_date);
  const { snackbar, setSnackbar } = useSnackbar()
  const navigate = useNavigate()
  const { authState, setAuthState } = useAuth()
  const [ buttonDisabled, setButtonDisabled ] = useState(false)
  const [ reviewData, setReviewData ] = useState<null | GenericForm>(null);

  const onClick = useHttpRequest(() => getSpecificReview(cardData.professional_id, cardData.service_id), value => {
    const newData = (value ?? { reviews: [] }).reviews as GenericForm[];
    setReviewData({ ...(newData[ 0 ] ?? {}) })
  }, "", true)

  useEffect(() => {
    // idc anymore
    if (Object.keys(reviewData ?? {}).length > 0) {
      setSnackbar({ message: "You have already posted a review for this service", severity: "warning" })
      setButtonDisabled(true)
    } else {
      if (reviewData !== null) {
        navigate(`/client/add-rating`, { state: { pro_id: cardData.professional_id, service_id: cardData.service_id } })
      }
    }
  }, [ reviewData ])

  return (
    <Grid item xs={"auto"} key={cardData.service_id}>
      <ServiceCard image={serviceAssets[ cardData.service_name ]} title={cardData.service_name} companyName={`${cardData.serviceOffers[ 0 ].first_name} ${(cardData.serviceOffers[ 0 ].last_name) ? cardData.serviceOffers[ 0 ].last_name : ""}`} price={`$${cardData.serviceOffers[ 0 ].offer_price}`} values={{
        "Booked Date": newDate.toLocaleDateString(),
        "Location": cardData.street_address,
        "Status": cardData.service_status
      }}>
        <Button disabled={buttonDisabled} onClick={onClick} sx={{ my: "12px" }} variant="contained">
          Review
        </Button>
      </ServiceCard>
    </Grid>
  )
}
