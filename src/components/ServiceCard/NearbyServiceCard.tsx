import ServiceCard from "./ServiceCard";
import { CardProps } from "../ServiceGrid/ServiceGrid";
import { Button, Grid } from "@mui/material";
import { useAuth } from "../../context/AuthContext";
import { useNavigate } from "react-router-dom";
import proRespondRequest from "../../services/services/proRespondRequest";
import { useSnackbar } from "../../context/SnackbarContext";
import { useLoading } from "../../context/LoadingContext";
import { useState } from "react";
import serviceAssets from "../../assets/serviceAssets/serviceAssets";
import useHttpRequest from "../../hooks/useHttpRequest";

export default function NearbyServiceCard({ cardData, setCardList }: CardProps) {
  const navigate = useNavigate()
  const [ isVisible, setIsVisible ] = useState(true)

  const viewDetails = (serviceId: number) => {
    navigate(`/pro/request/${serviceId}`)
  }

  const declineRequest = useHttpRequest(() => proRespondRequest({
    accept: false,
    service_id: cardData.service_id,
    offer_price: 0
  }), () => { }, "Succesfully rejected request")

  const newDate = new Date(cardData.service_request_date);
  return (
    <Grid item xs={"auto"} key={cardData.service_id}>
      <ServiceCard
        image={serviceAssets[ cardData.service_name ]}
        isVisible={isVisible}
        title={cardData.service_name}
        values={{
          "Booked Date": newDate.toLocaleDateString(),
          Location: cardData.street_address,
          "Expected Price": `$${cardData.expected_price}`,
          Status: cardData.service_status,
        }}
      >
        <Button onClick={() => { viewDetails(cardData.service_id) }}>View Details</Button>
        <Button onClick={() => { declineRequest() }}>Decline Request</Button>
      </ServiceCard>
    </Grid>
  )
}
