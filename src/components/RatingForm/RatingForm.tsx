import {
  ChangeEventHandler,
  FormEventHandler,
  ReactNode,
} from "react";
import { GenericForm } from "../../types/GenericForm";
import FormContainer from "../FormContainer/FormContainer";
import Rating from "@mui/material/Rating";
import { TextField } from "@mui/material";

interface Props {
  title: string;
  values?: GenericForm;
  setValues?: React.Dispatch<React.SetStateAction<GenericForm>>;
  readonly?: boolean;
  onSubmit?: FormEventHandler<HTMLFormElement>;
  onChange?: ChangeEventHandler<HTMLInputElement>;
  children?: ReactNode;
}

export default function RatingForm({
  values = { rating: 0 },
  setValues,
  readonly = false,
  title,
  onSubmit,
  children,
}: Props) {
  const handleChange = (event: any) => {
    let value = event.target.value;
    if (setValues) {
      setValues({
        ...values,
        [ event.target.name ]: value,
      });
    }
  };
  const handleRatingChange = (
    event: React.SyntheticEvent<Element, Event>,
    value: number | null
  ) => {
    if (setValues) {
      setValues({
        ...values,
        rating: value,
      });
    }
  };

  return (
    <FormContainer title={title} onSubmit={onSubmit}>
      <Rating
        value={values.rating ?? 0}
        size="large"
        name="rating"
        readOnly={readonly}
        onChange={handleRatingChange}
      ></Rating>
      <TextField
        multiline
        rows={4}
        label="Review"
        name="review"
        InputProps={{ readOnly: readonly }}
        InputLabelProps={{ shrink: true }}
        sx={{ my: "12px" }}
        onChange={handleChange}
        value={values.review ?? ""}
      ></TextField>
      {children}
    </FormContainer>
  );
}
