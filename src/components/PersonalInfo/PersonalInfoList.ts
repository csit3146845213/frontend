export const infolist: { [key: string]: any } = {
    email:{ label: "Email"},
    first_name: { label:"First name"},
    last_name: { label:"Last name", isRequired: false},
    home_number: { label: "Home Phone"},
    mobile:{ label: "Mobile"},
};
