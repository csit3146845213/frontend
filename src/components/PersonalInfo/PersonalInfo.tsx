import { Button, TextField } from "@mui/material";
import FormContainer from "../FormContainer/FormContainer";
import formatNumberInput from "../../hooks/formatNumberInput";
import { ChangeEvent, Dispatch, FormEvent, FormEventHandler, ReactNode, SetStateAction, useEffect, useState } from "react";
import { GenericForm } from "../../types/GenericForm";
import { infolist } from "./PersonalInfoList";
import { useAuth } from "../../context/AuthContext";
import validator from "validator";
import Address from "../Address/Address";

interface Props {
  personalInfo: GenericForm;
  setPersonalInfo: Dispatch<SetStateAction<GenericForm>>
  children?: ReactNode;
  onSubmit?: FormEventHandler<HTMLFormElement>;
  disableEmail?: boolean
}

export default function PersonalInfo({ personalInfo, setPersonalInfo, children, onSubmit = () => { }, disableEmail = false }: Props) {
  const [ inputList, setInputList ] = useState(structuredClone(infolist))
  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setPersonalInfo({
      ...personalInfo,
      [ event.target.name ]: event.target.value,
    });
  };

  const fieldValidators = (event: FormEvent<HTMLFormElement>) => {
    const newInputList = structuredClone(infolist)

    let hasError = false;
    if (!validator.isPostalCode((personalInfo.postcode).toString(), "any")) {
      hasError = true
      newInputList.postcode.error = true
      newInputList.postcode.errorMessage = "Not a valid postal code"
    }

    if (!validator.isMobilePhone((personalInfo.home_number).toString())) {
      hasError = true
      newInputList.home_number.error = true
      newInputList.home_number.errorMessage = "Not a valid number"
    }

    if (!validator.isMobilePhone((personalInfo.mobile).toString())) {
      hasError = true
      newInputList.mobile.error = true
      newInputList.mobile.errorMessage = "Not a valid phone number"
    }

    if (!validator.isEmail(personalInfo.email)) {
      hasError = true
      newInputList.email.error = true
      newInputList.email.errorMessage = "Not a valid email"
    }

    setInputList(newInputList)
    return hasError
  }

  const handleNumberChange = (event: ChangeEvent<HTMLInputElement>) => {
    setPersonalInfo((prev: any) => {
      const value = formatNumberInput(
        event.target.value,
        prev[ event.target.name ]
      );
      return {
        ...personalInfo,
        [ event.target.name ]: value,
      };
    });
  };

  const handleSubmit = (event: ChangeEvent<HTMLFormElement>) => {
    event.preventDefault()
    if (!fieldValidators(event)) {
      onSubmit(event)
    }
  }

  return (
    <FormContainer title={"Update details"} onSubmit={handleSubmit}>
      {Object.keys(inputList).map((value) => {
        return <TextField
          sx={{ mt: "12px" }}
          key={value}
          variant="outlined"
          name={value}
          multiline
          error={inputList[ value ].error}
          helperText={inputList[ value ].errorMessage}
          required={inputList[ value ].isRequired ?? true}
          rows={inputList[ value ].rows ?? 1}
          value={personalInfo[ value ] ?? ""}
          label={inputList[ value ].label}
          disabled={value === "email" ? disableEmail : false}
          onChange={inputList[ value ].isNumeric ? handleNumberChange : handleChange}
        />
      })}
      <Address formData={personalInfo} setFormData={setPersonalInfo}></Address>
      {children}
    </FormContainer>
  );
}
