import { TextField } from "@mui/material";

interface Props {
  name: string;
  label: string;
  required?: boolean;
  inputHandler?(e: { currentTarget: { name: any; value: any } }): void;
}
export default function TextForm({
  name,
  label,
  required,
  inputHandler,
}: Props) {
  return (
    <TextField
      variant="outlined"
      sx={{ mt: "12px" }}
      required={required}
      name={name}
      label={label}
      onChange={inputHandler}
    />
  );
}
