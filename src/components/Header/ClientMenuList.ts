export const ClientMenuList: {[key: string] : string} = {
    "membership" : "Membership",
    "client/services" : "Services",
    "client/orders" : "Orders"
}

export const ClientDropdownList: {[key: string] : string} = {
    "client/profile" : "Profile",
    "payment" : "Payment",
}
