export const ProMenuList: {[key: string] : string} = {
    "membership" : "Membership",
    "pro/request" : "Requests",
    "pro/orders" : "Orders"
}
export const ProDropdownList: {[key: string] : string} = {
    "pro/profile" : "Profile",
    "payment" : "Payment",
}
