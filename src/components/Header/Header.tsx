import {
  AppBar,
  Toolbar,
  IconButton,
  Box,
  styled,
  Button,
  Avatar,
  Container,
  Menu,
  MenuItem,
  LinearProgress,
} from "@mui/material";
import { Link, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { useAuth } from "../../context/AuthContext";
import AuthState, { UserTypes } from "../../types/AuthData";
import { ClientDropdownList, ClientMenuList } from "./ClientMenuList";
import { ProDropdownList, ProMenuList } from "./ProMenuList";
import { NotLoggedIn } from "./NotLoggedIn";
import userLogout from "../../services/userLogout";
import { useLoading } from "../../context/LoadingContext";
import { GenericForm } from "../../types/GenericForm";
import { AdminDropdownList, AdminMenuList } from "./AdminMenuList";

export default function Header() {
  const location = useLocation();
  const navigate = useNavigate();
  const { isLoading, setIsLoading } = useLoading();
  const { authState, setAuthState } = useAuth();
  const [ menuItems, setMenuItems ] = useState(NotLoggedIn);
  const [ dropdown, setDropdown ] = useState({} as GenericForm);
  const [ currentPath, setCurrentPath ] = useState(
    window.location.pathname.split("/").pop()
  );

  const [ anchorEl, setAnchorEl ] = useState<null | HTMLElement>(null);
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const logout = () => {
    setAuthState(userLogout())
    setAnchorEl(null);
    setMenuItems(NotLoggedIn)
    navigate("/")
  }

  useEffect(() => {
    const menuItems = {
      "Professional": ProMenuList,
      "Client": ClientMenuList,
      "Admin": AdminMenuList
    }
    const dropdownItems = {
      "Professional": ProDropdownList,
      "Client": ClientDropdownList,
      "Admin": AdminDropdownList
    }

    if (authState.isAuth && authState.userType) {
      setMenuItems(menuItems[ authState.userType ])
      setDropdown(dropdownItems[ authState.userType ])
    }
  }, [ authState ])

  useEffect(() => {
    setCurrentPath(window.location.pathname.split("/").pop());
  }, [ location ]);

  return (
    <AppBar
      position="sticky"
      color="secondary"
      sx={{
        backgroundColor: "white",
        borderBottom: "solid 1px white",
      }}
    >
      <Toolbar component={Container}>
        <Box
          sx={{ flexGrow: 1, display: "flex", justifyContent: "flex-start" }}
        >
          <Button
            component={Link}
            to="/"
            sx={{
              fontSize: "18px",
            }}
          >
            TradieLog
          </Button>
        </Box>
        {Object.keys(menuItems).map((value) => {
          return (
            <Button
              sx={{
                fontWeight:
                  currentPath === value.split("/").pop() ? "900" : "400",
              }}
              key={value}
              component={Link}
              to={value}
            >
              {menuItems[ value ]}
            </Button>
          );
        })}
        {authState.isAuth &&
          <IconButton onClick={handleClick} sx={{ p: 0, mx: "10px", display: authState.isAuth ? 'block' : 'none' }}>
            <Avatar>{((authState.userData ?? {}).first_name ?? "A")[ 0 ]}</Avatar>
          </IconButton>
        }
      </Toolbar>
      <Menu anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={handleClose}>
        {Object.keys(dropdown).map((value) => {
          return (
            <MenuItem
              key={value}
              component={Link}
              to={value}
              onClick={handleClose}
            >
              {dropdown[ value ]}
            </MenuItem>
          );
        })}
        <MenuItem onClick={logout}>
          Logout
        </MenuItem>
      </Menu>
      <LinearProgress sx={{
        display: isLoading ? 'block' : 'none'
      }} />
    </AppBar>


  );
}

