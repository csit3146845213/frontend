export const NotLoggedIn: {[key: string] : string} = {
    "membership": "Membership",
    "pro": "Become a Professional",
    "client/register": "Register",
    "login" : "Login"
}
