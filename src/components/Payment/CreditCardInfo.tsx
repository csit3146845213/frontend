import { Box, Button, Container, Typography } from '@mui/material';
type CreditCardProps = {
  // only last 4 digit
  creditCardNumber: string;
  name: string;
  // expiry date needs to be formatted into this form 'mm/yy' or '07/29'
  expiryDate: string;
  onClickCard?: (value: CreditCardProps) => void
};

const CreditCardInfo = ({ creditCardNumber, name, expiryDate, onClickCard }: CreditCardProps) => {
  let expiry = expiryDate.replace("&#x2F;", "/");
  return (
    <Container sx={{
      marginLeft: -4,
      my: "12px",
      textAlign: 'left'
    }}>
      <Box component={Button} onClick={() => {
        if (onClickCard) {
          onClickCard({
            creditCardNumber: creditCardNumber,
            name: name,
            expiryDate: expiryDate,
          })
        }
      }
      } sx={{ backgroundColor: '#F5F5F5', height: '220px', width: '420px', typography: 'h6', paddingLeft: 2, marginBottom: 0, textAlign: 'left' }}>
        <Box sx={{ padding: 0, width: "100%" }}>
          <Typography variant='h6'
            sx={{
              color: '#000',
              paddingTop: '100px',
              letterSpacing: 5
            }}>
            **** **** **** {creditCardNumber.substring(creditCardNumber.length - 4)}<br />
          </Typography>

          {name}<br />

          <Typography variant='body1'>
            {expiry}<br />
          </Typography>
        </Box>
      </Box>
    </Container>
  );
};
export type { CreditCardProps };
export default CreditCardInfo;
