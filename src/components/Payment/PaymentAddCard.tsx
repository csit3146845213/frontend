import { Box, Button, TextField } from "@mui/material";
import FormContainer from "../../components/FormContainer/FormContainer";
import CreditCardInfo, {
  CreditCardProps,
} from "../../components/Payment/CreditCardInfo";
import { ChangeEvent, Dispatch, SetStateAction, useState } from "react";
import { GenericForm } from "../../types/GenericForm";

// validate if its a number with 16 digit
function isValidCreditCardNumber(cardNumber: string): boolean {
  return /^\d{16}$/.test(cardNumber);
}

// validate whether it is a 3 digit number
function isValidCvc(cvc: string): boolean {
  return /^\d{3}$/.test(cvc);
}

// validate whether it is contain only string and space
function isValidName(name: string): boolean {
  const nameRegex = /^[a-zA-Z\s]*$/;
  return nameRegex.test(name);
}

// validate whether it is MM/YY format
function isValidExpiryDate(expiryDate: string) {
  // Check if the expiry date is in the format "MM/YY"
  if (!/^\d{2}\/\d{2}$/.test(expiryDate)) {
    return false;
  }

  // Split the expiry date into month and year
  const [ month, year ] = expiryDate.split("/").map((s) => parseInt(s));

  // Check if the year is in the past
  const currentYear = new Date().getFullYear() % 100;
  if (year < currentYear) {
    return false;
  }

  // Check if the month is between 1 and 12
  if (month < 1 || month > 12) {
    return false;
  }

  // If all checks pass, return true
  return true;
}

interface Props {
  formData: GenericForm
  setFormData: Dispatch<SetStateAction<GenericForm>>
  onSubmit: (event: React.FormEvent<HTMLFormElement>) => void,
  buttonText?: string
}

function PaymentAddCard({ formData, setFormData, onSubmit, buttonText = "Add Card" }: Props) {
  const [ creditCardNumberError, setCreditCardNumberError ] = useState("");
  const [ nameError, setNameError ] = useState("");
  const [ expiryDateError, setExpiryDateError ] = useState("");
  const [ cvcError, setCvcError ] = useState("");

  const validateFields = (event: React.FormEvent<HTMLFormElement>) => {
    let creditCardNumberError = "";
    let nameError = "";
    let expiryDateError = "";
    let cvcError = "";

    if (!formData.card_num) {
      creditCardNumberError = "Please enter a credit card number";
    } else if (!isValidCreditCardNumber(formData.card_num)) {
      creditCardNumberError = "Invalid credit card number";
    }

    if (!formData.card_name) {
      nameError = "Please enter a name";
    } else if (!isValidName(formData.card_name)) {
      nameError = "Invalid name";
    }

    if (!formData.expire_date) {
      expiryDateError = "Please enter an expiry date";
    } else if (!isValidExpiryDate(formData.expire_date)) {
      expiryDateError = "Invalid expiry date (use format MM/YY)";
    }

    if (!formData.ccv) {
      cvcError = "Please enter a CVC";
    } else if (!isValidCvc(formData.ccv)) {
      cvcError = "Invalid CVC";
    }

    setCreditCardNumberError(creditCardNumberError);
    setNameError(nameError);
    setExpiryDateError(expiryDateError);
    setCvcError(cvcError);

    if (!creditCardNumberError && !nameError && !expiryDateError && !cvcError) {
      return true
    }
  };

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (validateFields(event)) {
      onSubmit(event)
    }
  }

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const name = e.target.name;
    const value = e.target.value;
    setFormData((form) => ({ ...form, [ name ]: value }));
  };

  return (
    <FormContainer title="Bank Details" onSubmit={handleSubmit}>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "flex-start",
        }}
      >
        <TextField
          label="Credit card number"
          name="card_num"
          value={formData.card_num ?? ""}
          onChange={handleChange}
          error={Boolean(creditCardNumberError)}
          helperText={creditCardNumberError}
          sx={{ marginBottom: 2, width: 350 }}
        />
        <TextField
          label="Name on card"
          name="card_name"
          value={formData.card_name ?? ""}
          onChange={handleChange}
          error={Boolean(nameError)}
          sx={{ marginBottom: 2, width: "calc(50% - 8px)" }}
        />
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            width: "100%",
          }}
        >
          <TextField
            label="Expiry date (MM/YY)"
            value={formData.expire_date ?? ""}
            name="expire_date"
            onChange={handleChange}
            error={Boolean(expiryDateError)}
            sx={{ width: "calc(50% - 8px)", marginBottom: 2 }}
          />
          <TextField
            label="CVC"
            name="ccv"
            value={formData.ccv ?? ""}
            onChange={handleChange}
            error={Boolean(cvcError)}
            sx={{ width: "calc(50% - 8px)", marginBottom: 2 }}
          />
        </Box>
        <Button
          type="submit"
          variant="contained"
          color="button"
          sx={{ width: 300, marginBottom: 2 }}
        >
          {buttonText}
        </Button>
      </Box>
    </FormContainer>
  );
}

export default PaymentAddCard;
