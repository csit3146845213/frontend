import { Container, Box, Typography, Button } from "@mui/material";
import { ReactNode } from "react";
import { useNavigate } from "react-router-dom";

interface Props {
  title: string;
  subtitle: string;
  children?: ReactNode;
  imageUrl: string;
  isAuth?: boolean;
}

export default function NotSubscribedContainer({
  title,
  subtitle,
  children,
  imageUrl,
  isAuth = false
}: Props) {
  const navigate = useNavigate()

  return (
    <Container
      sx={{
        display: "flex",
        flexGrow: "1",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Box
        sx={{
          flexGrow: "1",
          padding: "20px",
          display: "flex",
          alignItems: "start",
          justifyContent: "center",
          flexDirection: "column",
        }}
      >
        <Typography fontWeight={"bold"} paddingY={"5px"} variant="h5">
          {title}
        </Typography>

        <Typography
          variant="h6"
          sx={{ fontFamily: "inherit", fontStyle: "italic" }}
        >
          {subtitle}
        </Typography>

        {children}

        {isAuth &&
          <Button onClick={() => { navigate("/pay-membership") }} variant="outlined" sx={{ backgroundColor: "#C7DBE7" }}>
            Get Membership
          </Button>
        }
      </Box>
      <Box
        sx={{
          flexGrow: "1",
          minWidth: "50%",
          padding: "20px",
          display: "flex",
          flexDirection: "column",
        }}
      >
        <img src={imageUrl} width={"600px"} height={"400px"}></img>
      </Box>
    </Container>
  );
}
