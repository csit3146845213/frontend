import { Box, Breadcrumbs, Container, Link, Typography } from "@mui/material";
import { Link as RouterLink } from "react-router-dom";
import { useBreadcrumb } from "../../context/BreadcrumbContext";
import { useEffect } from "react";

export default function Breadcrumb() {
  const breadcrumbContext = useBreadcrumb();
  const defaultBreadcrumbs = breadcrumbContext.breadcrumb;

  let hasPassedCurrent = false;
  let breadcrumbs: { [ key: string ]: string } = {};
  for (const i of Object.keys(defaultBreadcrumbs)) {
    if (i === location.pathname) {
      breadcrumbs[ "CURRENT" ] = defaultBreadcrumbs[ i ];
    } else {
      breadcrumbs[ i ] = defaultBreadcrumbs[ i ];
    }
  }
  const breadcrumbsKeys = Object.keys(breadcrumbs);

  return (
    <div>
      {breadcrumbsKeys.length > 0 && (
        <Box sx={{ py: "5px", borderBottom: "solid 1px black" }}>
          <Container>
            {/* Don't do this kids */}
            <Breadcrumbs separator="›" aria-label="breadcrumb">
              {[ ...breadcrumbsKeys ].map((value) => {
                if (hasPassedCurrent) return
                if (value === "CURRENT") {
                  hasPassedCurrent = true;
                  return (
                    <Typography
                      color={"text.primary"}
                      key={breadcrumbs[ value ]}
                      fontWeight={"bold"}
                    >
                      {breadcrumbs[ value ]}
                    </Typography>
                  );
                } else {
                  return (
                    <Link
                      underline="hover"
                      key={value}
                      color="inherit"
                      to={value}
                      component={RouterLink}
                    >
                      {breadcrumbs[ value ]}
                    </Link>
                  );
                }
              })}
            </Breadcrumbs>
          </Container>
        </Box>
      )}
    </div>
  );
}
