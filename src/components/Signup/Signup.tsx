import { Box, Button, Container, TextField, Typography } from "@mui/material";
import { ChangeEvent, FormEvent, ReactNode, useState } from "react";
import { Outlet, useNavigate, useOutletContext } from "react-router-dom";
import formatNumberInput from "../../hooks/formatNumberInput";
import FormContainer, { useFormContext } from "../FormContainer/FormContainer";
import PasswordForm from "../PasswordForm/PasswordForm";
import { infolist } from "../PersonalInfo/PersonalInfoList";
import { GenericForm } from "../../types/GenericForm";
import validator from "validator";
import Address from "../Address/Address";

interface Props {
  formData: GenericForm
  setFormData: React.Dispatch<React.SetStateAction<GenericForm>>
  onSubmit: React.FormEventHandler<HTMLFormElement>
  children?: ReactNode
}

function Signup({ formData, setFormData, onSubmit, children }: Props) {
  const [ inputList, setInputList ] = useState(structuredClone(infolist))
  const [ { confirmPassword, errorMessage }, setConfirmPassword ] = useState({
    confirmPassword: "",
    errorMessage: "",
  });

  const handleChange = (e: { currentTarget: { name: any; value: any } }) => {
    const name = e.currentTarget.name;
    const value = e.currentTarget.value;
    setFormData((form) => ({ ...form, [ name ]: value }));
  };

  const handleNumberChange = (event: ChangeEvent<HTMLInputElement>) => {
    setFormData((prev: any) => {
      const value = formatNumberInput(
        event.target.value,
        prev[ event.target.name ]
      );
      return {
        ...formData,
        [ event.target.name ]: value,
      };
    });
  };

  const fieldValidators = (event: FormEvent<HTMLFormElement>) => {
    const newInputList = structuredClone(infolist)

    let hasError = false;
    if (!validator.isPostalCode((formData.postcode).toString(), "any")) {
      newInputList.postcode.error = true
      newInputList.postcode.errorMessage = "Not a valid postal code"
    }

    if (formData.home_number !== undefined && !validator.isMobilePhone(formData.home_number)) {
      newInputList.home_number.error = true
      newInputList.home_number.errorMessage = "Not a valid number"
    }

    if (!validator.isMobilePhone(formData.mobile)) {
      newInputList.mobile.error = true
      newInputList.mobile.errorMessage = "Not a valid phone number"
    }

    if (!validator.isEmail(formData.email)) {
      newInputList.email.error = true
      newInputList.email.errorMessage = "Not a valid email"
    }

    setInputList(newInputList)
    return hasError
  }

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    if (!fieldValidators(event)) {
      onSubmit(event)
    }
  }

  const confirmPasswordValidator = (e: {
    currentTarget: { name: any; value: any };
  }) => {
    const value = e.currentTarget.value;
    let errorMessage = "";
    if (formData.password !== value) {
      errorMessage = "Password does not match";
    }
    setConfirmPassword({
      confirmPassword: value,
      errorMessage: errorMessage,
    });
  };

  return (
    <FormContainer title={"Sign Up"} onSubmit={handleSubmit}>
      <Box
        sx={{
          width: "100%",
          flexDirection: "column",
          display: "flex",
        }}
      >
        {[ ...Object.keys(inputList) ].map((value) => {
          return (
            <TextField
              sx={{ mt: "12px" }}
              key={value}
              variant="outlined"
              name={value}
              error={inputList[ value ].error ?? false}
              helperText={inputList[ value ].errorMessage}
              required={inputList[ value ].isRequired ?? true}
              multiline
              rows={inputList[ value ].rows ?? 1}
              value={formData[ value ] ?? ""}
              label={inputList[ value ].label}
              onChange={inputList[ value ].isNumeric ? handleNumberChange : handleChange}
            />
          );
        })}
        <Address formData={formData} setFormData={setFormData}></Address>
        <PasswordForm value={formData.password} onChange={handleChange}></PasswordForm>
        <PasswordForm
          error={formData.password !== confirmPassword}
          onChange={confirmPasswordValidator}
          errorMsg={errorMessage}
          label="Confirm Password"
          name="confirm"
        ></PasswordForm>
        {children}
      </Box>
    </FormContainer>
  );
}

export default Signup;
