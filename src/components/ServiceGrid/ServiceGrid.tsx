import { Grid, Button, Container, Typography } from "@mui/material";
import React, { Dispatch, SetStateAction, useEffect, useState } from "react";
import { GenericForm } from "../../types/GenericForm";

export interface CardProps {
  cardData: GenericForm
  setCardList: Dispatch<SetStateAction<GenericForm[]>>
}

interface Props {
  title?: string
  serviceData: GenericForm[]
  setServiceData: Dispatch<SetStateAction<GenericForm[]>>
  cardType: ({ cardData, setCardList }: CardProps) => JSX.Element
}

export default function ServiceGrid({ serviceData, title, setServiceData, cardType }: Props) {
  return (
    <Container sx={{ minHeight: "30vh" }}>
      <Typography textAlign={"left"} variant="h6" sx={{ my: "12px" }}>
        {title}
      </Typography>
      <Grid container spacing={2} sx={{ mt: "12px" }}>
        {serviceData.length == 0 ? `No services found` : ""}
        {serviceData.map((value: GenericForm) => {
          const cardProps: CardProps = {
            cardData: value,
            setCardList: setServiceData
          }
          return (<React.Fragment key={`service-card-${value.service_id}`}>
            {cardType(cardProps)}
          </React.Fragment>
          )
        })}
      </Grid>
    </Container>
  )
}
