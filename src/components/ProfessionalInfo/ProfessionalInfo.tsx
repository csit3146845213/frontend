import {
  Box,
  Breadcrumbs,
  Button,
  FormControl,
  FormHelperText,
  InputLabel,
  Link,
  MenuItem,
  Select,
  SelectChangeEvent,
  TextField,
  Typography,
} from "@mui/material";
import FormContainer from "../../components/FormContainer/FormContainer";
import {
  ChangeEventHandler,
  FormEvent,
  MouseEvent,
  ReactNode,
  useEffect,
  useRef,
  useState,
} from "react";
import DeletableEntry from "../../components/DeletableEntry/DeletableEntry";
import { GenericForm } from "../../types/GenericForm";

interface Props {
  setSkillList: React.Dispatch<React.SetStateAction<Set<string>>>;
  setFormData: React.Dispatch<React.SetStateAction<GenericForm>>;
  formData: GenericForm
  skillList: Set<string>;
  children?: ReactNode;
  onSubmit: (event: FormEvent<HTMLFormElement>) => void
}

export default function ProfessionalInfo({ children, formData, setFormData, skillList, setSkillList, onSubmit }: Props) {
  const [ selectedSkill, setSelectedSkill ] = useState("");
  const [ skillListError, setSkillListError ] = useState("");
  const [ descriptionError, setDescriptionError ] = useState("");
  const selectSkill = (event: SelectChangeEvent) => {
    setSelectedSkill(event.target.value);
  };

  const handleChange = (e: { currentTarget: { name: any; value: any } }) => {
    const name = e.currentTarget.name;
    const value = e.currentTarget.value;

    setFormData((form) => ({ ...form, [ name ]: value }));
  };

  const addNewSkill = () => {
    if (!selectedSkill) return;
    setSkillList((prev) => {
      const temp = new Set(prev);
      temp.add(selectedSkill);

      return temp;
    });
  };

  const deleteSkill = (e: MouseEvent<HTMLButtonElement>) => {
    setSkillList((prev) => {
      const temp = new Set(prev);
      temp.delete(e.currentTarget.value);
      return temp;
    });
  };

  const fieldValidators = (event: FormEvent<HTMLFormElement>) => {
    setSkillListError("")
    setDescriptionError("")
    let hasError = false;
    if (skillList.size == 0) {
      hasError = true;
      setSkillListError("At least 1 skill has to be chosen")
    }
    if (formData.description.length == 0 || formData.description === undefined) {
      hasError = true;
      setDescriptionError("Description cannot be empty")
    }
    return hasError
  }

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (!fieldValidators(event))
      onSubmit(event)
  }

  return (
    <FormContainer title={"Update Professional Info"} onSubmit={handleSubmit}>
      <TextField
        sx={{ mt: "12px" }}
        variant="outlined"
        name="description"
        label="Description"
        required
        value={formData.description}
        onChange={handleChange}
        multiline
        helperText={descriptionError}
        error={Boolean(descriptionError.length > 0)}
        rows={4}
        InputLabelProps={{
          shrink: true
        }}
      />
      <Box sx={{ mt: "12px" }} display={"flex"} flexDirection={"row"}>
        <FormControl fullWidth>
          <InputLabel>Skills</InputLabel>
          <Select value={selectedSkill} onChange={selectSkill} name="skill"
            error={Boolean(skillListError.length > 0)} label="Skills">
            <MenuItem value={"Tree Removal"}>Tree removal</MenuItem>
            <MenuItem value={"Roof Cleaning"}>Roof cleaning</MenuItem>
            <MenuItem value={"Fence Installation"}>Fence installation</MenuItem>
            <MenuItem value={"Oven Repairs"}>Oven repairs</MenuItem>
            <MenuItem value={"Lawn Mowing"}>Lawn mowing</MenuItem>
          </Select>
          <FormHelperText>{skillListError}</FormHelperText>
        </FormControl>
        <Button
          disabled={!Boolean(selectedSkill)}
          onClick={addNewSkill}
          variant="outlined"
          sx={{ ml: "12px", maxHeight: "58px" }}
        >
          Add
        </Button>
      </Box>
      {[ ...skillList ].map((value) => (
        <DeletableEntry
          key={value}
          value={value}
          onClickDelete={deleteSkill}
        ></DeletableEntry>
      ))}
      {children}
    </FormContainer>
  );
}
