import { Box, Button, Container, TextField, Typography } from "@mui/material";

import { FormEventHandler, ReactNode, useState } from "react";
import { GenericForm } from "../../types/GenericForm";
import { useOutletContext } from "react-router-dom";

interface Props {
  title: string;
  children?: ReactNode;
  onSubmit?: FormEventHandler<HTMLFormElement>;
}

export default function FormContainer({ title, children, onSubmit }: Props) {
  return (
    <Container
      sx={{
        display: "flex",
        flexGrow: "1",
        flexDirection: "row",
        marginY: "10px",
        border: "solid 1px black",
      }}
    >
      <Box
        sx={{
          flexGrow: "1",
          padding: "20px",
          display: "flex",
          alignItems: "start",
          justifyContent: "center",
          flexDirection: "column",
        }}
      >
        <Typography fontWeight={"bold"} paddingY={"30px"} variant="h5">
          {title}
        </Typography>
        <form onSubmit={onSubmit} className="w-full">
          <Box
            sx={{
              width: "100%",
              flexDirection: "column",
              display: "flex",
            }}
          >
            {children}
          </Box>
        </form>
      </Box>
      <Box
        sx={{
          flexGrow: "1",
          minWidth: "50%",
          padding: "20px",
          display: "flex",
          flexDirection: "column",
        }}
      ></Box>
    </Container>
  );
}

export function useFormContext() {
  return useOutletContext<[ GenericForm, React.Dispatch<React.SetStateAction<GenericForm>> ]>();
}
