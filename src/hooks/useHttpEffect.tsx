import { useEffect } from "react"
import { useLoading } from "../context/LoadingContext"
import { useSnackbar } from "../context/SnackbarContext"

export default function useHttpEffect(httpRequest: Promise<any>, onSuccess: (value: any) => void, dependencyList: React.DependencyList = []) {
    const { isLoading, setIsLoading } = useLoading()
    const { snackbar, setSnackbar } = useSnackbar()

    useEffect(() => {
        setIsLoading(true)
        httpRequest
            .then(onSuccess)
            .catch((value) => {
                const severity = value.status >= 500 ? "error" : "warning"
                setSnackbar({ message: value.message, severity: severity })
            })
            .finally(() => {
                setIsLoading(false)
            })
    }, dependencyList)
}
