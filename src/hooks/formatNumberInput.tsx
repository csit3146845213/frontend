export default function formatNumberInput(newStr: string, prev: number) {
  const number = Number(newStr);
  if (number > 9007199254740992) return prev;
  if (isNaN(number)) return prev;
  return number;
}
