import React, {
  ReactNode,
  createContext,
  useContext,
  useEffect,
  useState,
} from "react";

interface Props {
  children?: ReactNode;
}

const BreadcrumbContext = createContext<any>({});
function BreadcrumbProvider({ children }: Props) {
  const [ breadcrumb, setBreadcrumbState ] = useState<any>({});

  useEffect(() => {
    if (Object.keys(breadcrumb).length === 0) return;
  }, [ breadcrumb, setBreadcrumbState ]);

  return (
    <BreadcrumbContext.Provider value={{ breadcrumb, setBreadcrumbState }}>
      {children}
    </BreadcrumbContext.Provider>
  );
}

export function useBreadcrumb() {
  return useContext(BreadcrumbContext);
}

export default BreadcrumbProvider;
