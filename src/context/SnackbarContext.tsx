import { AlertColor } from "@mui/material";
import React, {
  Dispatch,
  ReactNode,
  SetStateAction,
  createContext,
  useContext,
  useEffect,
  useState,
} from "react";

interface Props {
  children?: ReactNode;
}

const SnackbarContext = createContext<any>(false);
function SnackbarProvider({ children }: Props) {
  const [ snackbar, setSnackbar ] = useState(
    false
  );

  return (
    <SnackbarContext.Provider value={{ snackbar, setSnackbar }}>
      {children}
    </SnackbarContext.Provider>
  );
}

export interface SnackbarProps {
  message: string | undefined
  severity: AlertColor
}

export function useSnackbar() {
  const { snackbar, setSnackbar }: { snackbar: SnackbarProps, setSnackbar: Dispatch<SetStateAction<SnackbarProps>> } = useContext(SnackbarContext)
  return { snackbar, setSnackbar };
}

export default SnackbarProvider;
