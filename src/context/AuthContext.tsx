import React, {
  ReactNode,
  createContext,
  useContext,
  useEffect,
  useState,
} from "react";
import AuthState from "../types/AuthData";
import axios from "axios";
import { Config, baseUrl } from "../data/Config";

axios.defaults.headers.common[ "Content-Type" ] = "application/json";
axios.defaults.baseURL = baseUrl;

interface Props {
  children?: ReactNode;
}

const AuthContext = createContext<any>(AuthState.createFromString(""));
function AuthProvider({ children }: Props) {
  const [ authState, setAuthState ] = useState(
    AuthState.createFromString(localStorage.getItem("loggedUser") || "")
  );
  return (
    <AuthContext.Provider value={{ authState, setAuthState }}>
      {children}
    </AuthContext.Provider>
  );
}

export function useAuth() {
  const { authState, setAuthState }: { authState: AuthState, setAuthState: React.Dispatch<React.SetStateAction<AuthState>> } = useContext(AuthContext)
  return { authState, setAuthState };
}

export default AuthProvider;
