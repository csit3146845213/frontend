import React, {
  Dispatch,
  ReactNode,
  SetStateAction,
  createContext,
  useContext,
  useEffect,
  useState,
} from "react";
import AuthState from "../types/AuthData";

interface Props {
  children?: ReactNode;
}

const LoadingContext = createContext<any>(false);
function LoadingProvider({ children }: Props) {
  const [ isLoading, setIsLoading ] = useState(
    false
  );

  return (
    <LoadingContext.Provider value={{ isLoading, setIsLoading }}>
      {children}
    </LoadingContext.Provider>
  );
}

export function useLoading() {
  const { isLoading, setIsLoading }: { isLoading: boolean, setIsLoading: Dispatch<SetStateAction<boolean>> } = useContext(LoadingContext)
  return { isLoading, setIsLoading };
}

export default LoadingProvider;
